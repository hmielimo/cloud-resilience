terraform {
  required_providers {
    oci = {
      source = "oracle/oci"
    }
    template = {
      source = "hashicorp/template"
    }
  }
  required_version = ">= 0.13"
}
