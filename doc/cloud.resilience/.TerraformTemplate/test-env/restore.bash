#!/bin/bash
#
# (c) 2022 Heinz Mielimonka Oracle - heinz.mielimonka@oracle.com - feel free to reuse and share
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# SDK and CLI Configuration File - https://docs.oracle.com/en-us/iaas/Content/API/Concepts/sdkconfig.htm
# Oracle Cloud Infrastructure CLI Command Reference - https://docs.oracle.com/iaas/tools/oci-cli/latest/oci_cli_docs/
# OCI Block Storage crossregion volume group replication - https://blogs.oracle.com/cloud-infrastructure/post/announcing-oci-block-storage-cross-region-volume-group-replication
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
#
# some examples
# =============
# get JSON File to feed into jq
# oci --profile ba bv volume-group-backup list --all --compartment-id ocid1.compartment.oc1..aaaaaaaageojzaogxd3ko43ycjgitytvsyg2p6e7hvfjvx4bcdgkemz26t5q --display-name VGasmdisk0
#
# get number of different timestamps
# jq --raw-output '[.data[] | select(."lifecycle-state"=="AVAILABLE")] | length'
# get list of different timestamps
# jq --raw-output '.data[] | select(."lifecycle-state"=="AVAILABLE") | ."freeform-tags"."Timestamp"'
#
# bEDy:EU-FRANKFURT-1-AD-1
# bEDy:EU-FRANKFURT-1-AD-2
# bEDy:EU-FRANKFURT-1-AD-3
#
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# Set Colors for output
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
Color_Off='\e[0m'       # Text Reset
Black='\e[0;30m'        # Black
Red='\e[0;31m'          # Red
Green='\e[0;32m'        # Green
Yellow='\e[0;33m'       # Yellow
Blue='\e[0;34m'         # Blue
Purple='\e[0;35m'       # Purple
Cyan='\e[0;36m'         # Cyan
White='\e[0;37m'        # White

# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# Script status output
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Color_Off"
echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") : (0) Start script restore.bash"
echo -e "$Green ================================================================================================================================"

# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# START needed functions and variables
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
if [[ 1 == 1 ]]; then

PROFILE=ba
# the compartment to work with
COMPARTMENT_ID="###IT_SHOULD_BE_OBVIOUS_THIS_MUST_BE_REPLACED_BY_THE_REAL_COMPARTMENT_OCID####"

# name of resource we look for when checking for savepoints in source compartments
VGZERO="VGasmdisk0"

# name of subcompartment for backups
BCKP_COMPARTMENT_NAME="Sicherungen"
# string will be appended to compartment name to distinguish the backup compartment and work compartment
APPENDIX="BCKP"

# Timestamp
TIMESTAMP=$(date "+%Y%m%d%H%M%S")

# generate a unique ID to tag resources created by this script
UNIQUE_ID="k4JgHrt${TIMESTAMP}"
if [ -e /dev/urandom ];then
 UNIQUE_ID=$(cat /dev/urandom|LC_CTYPE=C tr -dc "[:alnum:]"|fold -w 32|head -n 1)
fi

# array keeps a list of temporary files to cleanup
TMP_FILE_LIST=()

# do not change value if set in environment
DEBUG_PRINT="${DEBUG_PRINT:=false}"

# filename of this script
THIS_SCRIPT="$(basename ${BASH_SOURCE})"

# Do cleanup
function Cleanup() {
  for i in "${!TMP_FILE_LIST[@]}"; do
    if [ -f "${TMP_FILE_LIST[$i]}" ]; then
      debug_print "deleting ${TMP_FILE_LIST[$i]}"
      rm -f "${TMP_FILE_LIST[$i]}"
    fi
  done
}

# Do cleanup, display error message and exit
function Interrupt() {
  Cleanup
  exitcode=99
  echo -e "\nScript '${THIS_SCRIPT}' aborted by user. $Color_Off"
  exit $exitcode
}

# trap ctrl-c and call interrupt()
trap Interrupt INT
# trap exit and call cleanup()
trap Cleanup   EXIT

debug_print()
{
  if ${DEBUG_PRINT}; then
    echo -e "$Red ......# $1 $Color_Off"
  fi
}

# call:
# tempfile my_temp_file
# to create a tempfile. The generated file is $my_temp_file
function tempfile()
{
  local __resultvar=$1
  local __tmp_file=$(mktemp -t ${THIS_SCRIPT}_tmp_file.XXXXXXXXXXX) || {
    echo -e "$Cyan ......#*** Creation of ${__tmp_file} failed $Color_Off";
    exit 1;
  }
  TMP_FILE_LIST+=("${__tmp_file}")
  if [[ "$__resultvar" ]]; then
    eval $__resultvar="'$__tmp_file'"
  else
    echo -e "$Cyan ......#$__tmp_file"
  fi
}

         FROM_PARAM="--from"
    SAVEPOINT_PARAM="--savepoint"
AUTOSAVEPOINT_PARAM="--autosavepoint"

usage()
{
  echo -e "\nUsage :"
  echo -e "        $(basename $0)"
  echo -e "        to list the possible restore points (available backups) for the current compartment\n"
  echo -e "        $(basename $0) ${FROM_PARAM} <compartment_name>"
  echo -e "        to list the possible restore points (available backups) from another compartment\n"
  echo -e "        $(basename $0) [${FROM_PARAM} <compartment_name>] ${SAVEPOINT_PARAM} <savepoint>"
  echo -e "        to restore from a restore point [from another compartment] (restore manual backup for boot and block volumes)\n"
  echo -e "        $(basename $0) [${FROM_PARAM} <compartment_name>] ${AUTOSAVEPOINT_PARAM} <savepoint>"
  echo -e "        to restore from a restore point [from another compartment] (restore manual backup for boot volumes and make use of crossregion volume group replication for block volumes\n $Color_Off"
  exit
}

case "$#" in
    0)
      unset SOURCE_COMPARTMENT
      unset SAVEPOINT
      ;;
    2)
      case "$1" in
          "${FROM_PARAM}" | "${SAVEPOINT_PARAM}" | "${AUTOSAVEPOINT_PARAM}")
            if [[ "$1" == "${FROM_PARAM}" ]]; then
              SOURCE_COMPARTMENT="$2"
              unset SAVEPOINT
			  AUTOSAVEPOINT="false"
            fi
			if [[ "$1" == "${SAVEPOINT_PARAM}" ]]; then
              unset SOURCE_COMPARTMENT
              SAVEPOINT="$2"
			  AUTOSAVEPOINT="false"
            fi
		    if [[ "$1" == "${AUTOSAVEPOINT_PARAM}" ]]; then
              unset SOURCE_COMPARTMENT
              SAVEPOINT="$2"
			  AUTOSAVEPOINT="true"
            fi
            ;;
          *)
            usage;
      esac
      ;;
    4)
      case "$1" in
          "${FROM_PARAM}")
            if [[ "$3" != "${SAVEPOINT_PARAM}" && "$3" != "${AUTOSAVEPOINT_PARAM}" ]]; then
              usage
            fi
 			if [[ "$3" == "${SAVEPOINT_PARAM}" ]]; then
              SOURCE_COMPARTMENT="$2"
              SAVEPOINT="$4"
			  AUTOSAVEPOINT="false"
            fi
		    if [[ "$3" == "${AUTOSAVEPOINT_PARAM}" ]]; then
              SOURCE_COMPARTMENT="$2"
              SAVEPOINT="$4"
			  AUTOSAVEPOINT="true"
            fi
            ;;
         *)
            usage;
      esac
      ;;
    *)
      usage;
esac
debug_print "#### Parameter provided:"
debug_print "      SOURCE_COMPARTMENT : \"${SOURCE_COMPARTMENT}\""
debug_print "               SAVEPOINT : \"${SAVEPOINT}\"\n"
debug_print "           AUTOSAVEPOINT : \"${AUTOSAVEPOINT}\"\n"
fi

 
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# Script status output
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") : (1) GET information about the environment"
echo -e "$Green ================================================================================================================================"
if [[ 1 == 1 ]]; then

# get information about the current compartment
tempfile CURRENT_COMPARTMENT
oci --profile "${PROFILE}" iam compartment get --compartment-id "${COMPARTMENT_ID}" > "${CURRENT_COMPARTMENT}"
COMPARTMENT_NAME="$(cat "${CURRENT_COMPARTMENT}"|jq --raw-output '.data."name"')"
PARENT_COMPARTMENT_ID="$(cat "${CURRENT_COMPARTMENT}"|jq --raw-output '.data."compartment-id"')"
echo -e "$Cyan ...# current compartment name : $COMPARTMENT_NAME"
debug_print "parent_compartment_id : $PARENT_COMPARTMENT_ID"

# get information about the parent compartment
tempfile PARENT_COMPARTMENT
oci --profile "${PROFILE}" iam compartment get --compartment-id "${PARENT_COMPARTMENT_ID}" > "${PARENT_COMPARTMENT}"
PARENT_COMPARTMENT_NAME="$(cat "${PARENT_COMPARTMENT}"|jq --raw-output '.data."name"')"
PARENT_COMPARTMENT_ID="$(cat "${PARENT_COMPARTMENT}"|jq --raw-output '.data."id"')"
debug_print "parent compartment name : $PARENT_COMPARTMENT_NAME"
debug_print "parent_compartment_id : $PARENT_COMPARTMENT_ID"

# get the compartment used for backups e.g. "Sicherungen"
tempfile COMPARTMENT_LIST
oci --profile "${PROFILE}" iam compartment list --all --compartment-id "${PARENT_COMPARTMENT_ID}" --lifecycle-state active --name "${BCKP_COMPARTMENT_NAME}" > "${COMPARTMENT_LIST}"
num_compartments=$(( $(cat "${COMPARTMENT_LIST}"|jq --raw-output '.data | length') + 0 ))
if [[ $num_compartments != 1 ]]; then
  echo -e "$Cyan ...# Could not identify the Compartment used for backups $Color_Off"
  exit
fi
BCKP_COMPARTMENT_ID="$(cat "${COMPARTMENT_LIST}"|jq --raw-output '.data[0]."id"')"


# create a backup subcompartment in backup compartment for current compartment
if [ -z ${SOURCE_COMPARTMENT+x} ]; then
  # SOURCE_COMPARTMENT was not set
  MY_BCKP_COMPARTMENT_NAME="${COMPARTMENT_NAME}${APPENDIX}"
else
  MY_BCKP_COMPARTMENT_NAME="${SOURCE_COMPARTMENT}${APPENDIX}"
fi
echo -e "$Cyan ...# MY_BCKP_COMPARTMENT_NAME : ${MY_BCKP_COMPARTMENT_NAME}"
tempfile COMPARTMENT_LIST
oci --profile "${PROFILE}" iam compartment list --all --compartment-id "${BCKP_COMPARTMENT_ID}" --lifecycle-state active --name "${MY_BCKP_COMPARTMENT_NAME}" > "${COMPARTMENT_LIST}"
num_compartments=$(( $(cat "${COMPARTMENT_LIST}"|jq --raw-output '.data | length') + 0 ))
if [[ $num_compartments != 1 ]]; then
  echo -e "$Cyan ...# the backup compartment \"$MY_BCKP_COMPARTMENT_NAME\" does not exist in \"$BCKP_COMPARTMENT_NAME\" $Color_Off"
  exit
else
  echo -e "$Cyan ...# found \"$MY_BCKP_COMPARTMENT_NAME\" below the \"$BCKP_COMPARTMENT_NAME\" compartment"
fi
# get ocid of backup compartment
tempfile COMPARTMENT_LIST
oci --profile "${PROFILE}" iam compartment list --all --compartment-id "${BCKP_COMPARTMENT_ID}" --lifecycle-state active --name "${MY_BCKP_COMPARTMENT_NAME}" > "${COMPARTMENT_LIST}"
num_compartments=$(( $(cat "${COMPARTMENT_LIST}"|jq --raw-output '.data | length') + 0 ))
if [[ $num_compartments != 1 ]]; then
  echo -e "\nthere is more than one or no compartment \"${MY_BCKP_COMPARTMENT_NAME}\" below \"$BCKP_COMPARTMENT_NAME\"\n\nGiving up. Exiting.\n $Color_Off"
  exit
fi
MY_BCKP_COMPARTMENT="$(cat "${COMPARTMENT_LIST}"|jq --raw-output '.data[0]."id"')"
debug_print "MY_BCKP_COMPARTMENT=${MY_BCKP_COMPARTMENT}"



tempfile SAVEPOINT_LIST
debug_print "oci --profile \"${PROFILE}\" bv volume-group-backup list --all --compartment-id \"${MY_BCKP_COMPARTMENT}\" --display-name \"${VGZERO}\" > \"${SAVEPOINT_LIST}\""
oci --profile "${PROFILE}" bv volume-group-backup list --all --compartment-id "${MY_BCKP_COMPARTMENT}" --display-name "${VGZERO}" > "${SAVEPOINT_LIST}"
num_savepoints=$(( $(cat "${SAVEPOINT_LIST}"|jq --raw-output '[.data[] | select(."lifecycle-state"=="AVAILABLE")] | length') + 0 ))
debug_print "Number of Savepoints : $num_savepoints"
if [[ $num_savepoints == 0 ]]; then
  echo -e "$Cyan ...# no savepoints in compartment ${MY_BCKP_COMPARTMENT_NAME} $Color_Off"
  exit
fi
if [ -z ${SAVEPOINT+x} ]; then
  # display the list of savepoints and exit
  echo -e "$Cyan ...# Available Savepoints in Compartment ${MY_BCKP_COMPARTMENT_NAME}: $Color_Off"
  cat "${SAVEPOINT_LIST}"|jq --raw-output '.data[] | select(."lifecycle-state"=="AVAILABLE") | ."freeform-tags"."Timestamp"'
  exit
else
  # check if the provided savepoint exists
  num_hits=$(( $(jq --raw-output '[.data[] | select(."freeform-tags"."Timestamp"=="'${SAVEPOINT}'" and ."lifecycle-state"=="AVAILABLE")] | length' "${SAVEPOINT_LIST}") + 0 ))
  debug_print "num_hits = $num_hits"
  if [[ $num_hits == 0 ]]; then
    echo -e "$Cyan ...# I could not find the savepoint $SAVEPOINT in compartment $MY_BCKP_COMPARTMENT_NAME $Color_Off"
    exit
  fi
  if [[ $num_hits > 1 ]]; then
    echo -e "$Cyan ...# savepoint $SAVEPOINT seems to exist more than once in compartment $MY_BCKP_COMPARTMENT_NAME $Color_Off"
    exit
  fi
fi
# source compartment and requested savepoint do exist at this point
fi


# start with housekeeping
# **************************************************************************************************************************************************************
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# Script status output
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") : (2) Housekeeping"
echo -e "$Green ================================================================================================================================"
if [[ 1 == 1 ]]; then

# delete all volume-group backups
echo -e "$Cyan ...# delete all volume-group backups"
tempfile BACKUP_LIST
debug_print "oci --profile \"${PROFILE}\" bv volume-group-backup list --all --compartment-id \"${COMPARTMENT_ID}\" | jq --raw-output '[.data[] | select(.\"lifecycle-state\"==\"AVAILABLE\")]' > \"${BACKUP_LIST}\""
oci --profile "${PROFILE}" bv volume-group-backup list --all --compartment-id "${COMPARTMENT_ID}" | jq --raw-output '[.data[] | select(."lifecycle-state"=="AVAILABLE")]' > "${BACKUP_LIST}"
num_vols=$(( $(jq --raw-output 'length' "${BACKUP_LIST}") + 0))
debug_print "will delete $num_vols volume group backups"
for ((i=0; i<$num_vols; i++)); do
  BV_ID=$(jq --raw-output '.['$i'] | ."id"' "${BACKUP_LIST}")
  debug_print "oci --profile \"${PROFILE}\" bv volume-group-backup delete --force --volume-group-backup-id \"${BV_ID}\" --wait-for-state TERMINATED"
  oci --profile "${PROFILE}" bv volume-group-backup delete --force --volume-group-backup-id "${BV_ID}" --wait-for-state TERMINATED
done

# delete all Volume Backups in the current compartment
echo -e "$Cyan ...# delete all Volume Backups in the current compartment"
debug_print "oci --profile \"${PROFILE}\" bv backup list --all --compartment-id \"${COMPARTMENT_ID}\" | jq --raw-output '[.data[] | select(.\"lifecycle-state\"==\"AVAILABLE\")]' > \"${BACKUP_LIST}\""
oci --profile "${PROFILE}" bv backup list --all --compartment-id "${COMPARTMENT_ID}" | jq --raw-output '[.data[] | select(."lifecycle-state"=="AVAILABLE")]' > "${BACKUP_LIST}"
num_vols=$(( $(jq --raw-output 'length' "${BACKUP_LIST}") + 0))
debug_print "will delete $num_vols volume backups"
for ((i=0; i<$num_vols; i++)); do
  BV_ID=$(jq --raw-output '.['$i'] | ."id"' "${BACKUP_LIST}")
  debug_print "oci --profile \"${PROFILE}\" bv backup delete --force --volume-backup-id \"${BV_ID}\" --wait-for-state TERMINATED"
  oci --profile "${PROFILE}" bv backup delete --force --volume-backup-id "${BV_ID}" --wait-for-state TERMINATED
done

# delete all Boot Volume Backups in the current compartment
echo -e "$Cyan ...# delete all Boot Volume Backups in the current compartment"
debug_print "oci --profile \"${PROFILE}\" bv boot-volume-backup list --all --compartment-id \"${COMPARTMENT_ID}\" | jq --raw-output '[.data[] | select(.\"lifecycle-state\"==\"AVAILABLE\")]' > \"${BACKUP_LIST}\""
oci --profile "${PROFILE}" bv boot-volume-backup list --all --compartment-id "${COMPARTMENT_ID}" | jq --raw-output '[.data[] | select(."lifecycle-state"=="AVAILABLE")]' > "${BACKUP_LIST}"
num_vols=$(( $(jq --raw-output 'length' "${BACKUP_LIST}") + 0))
debug_print "will delete $num_vols boot volume backups"
for ((i=0; i<$num_vols; i++)); do
  BV_ID=$(jq --raw-output '.['$i'] | ."id"' "${BACKUP_LIST}")
  debug_print "oci --profile \"${PROFILE}\" bv boot-volume-backup delete --force --boot-volume-backup-id \"${BV_ID}\" --wait-for-state TERMINATED"
  oci --profile "${PROFILE}" bv boot-volume-backup delete --force --boot-volume-backup-id "${BV_ID}" --wait-for-state TERMINATED
done
# **************************************************************************************************************************************************************
# end of housekeeping
fi


# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# Script status output
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") : (3) CREATE Boot volume backups"
echo -e "$Green ================================================================================================================================"
if [[ 1 == 1 ]]; then

# generate array of availability domain names
tempfile AD_LIST_JSON
AD_NAME=()
oci --profile "${PROFILE}" iam availability-domain list --all > "${AD_LIST_JSON}"
num_ads=$(( $(cat "${AD_LIST_JSON}"|jq --raw-output '.data | length') + 0 ))
debug_print "#ADs : $num_ads"
for ((i=0; i<$num_ads; i++)); do
  AD_NAME+=("$(cat "${AD_LIST_JSON}"|jq --raw-output '.data['$i']."name"')")
done
for i in "${!AD_NAME[@]}"; do
  debug_print "#### ${AD_NAME[$i]}"
done

# because boot volume backups can't be copied, create boot volumes from boot volume backups,
# create boot volume backups from the newly created boot volumes,
# move the newly created boot volume backups to the target compartment

# create boot volumes
echo -e "$Cyan ...# create boot volumes"
tempfile BOOT_VOLUMES
debug_print "oci --profile \"${PROFILE}\" bv boot-volume-backup list --all --compartment-id \"${MY_BCKP_COMPARTMENT}\" | jq --raw-output '[.data[] | select(.\"freeform-tags\".\"Timestamp\"==\"'${SAVEPOINT}'\" and .\"lifecycle-state\"==\"AVAILABLE\")]' > \"${BOOT_VOLUMES}\""
oci --profile "${PROFILE}" bv boot-volume-backup list --all --compartment-id "${MY_BCKP_COMPARTMENT}" | jq --raw-output '[.data[] | select(."freeform-tags"."Timestamp"=="'${SAVEPOINT}'" and ."lifecycle-state"=="AVAILABLE")]' > "${BOOT_VOLUMES}"
num_vols=$(( $(jq --raw-output 'length' "${BOOT_VOLUMES}") + 0))
debug_print "num_vols = $num_vols"
for ((i=0; i<$num_vols; i++)); do
  BV_ID=$(jq --raw-output '.['$i'] | ."id"' "${BOOT_VOLUMES}")
  BV_NAME=$(jq --raw-output '.['$i'] | ."display-name"' "${BOOT_VOLUMES}")
  BV_AD=$(jq --raw-output '.['$i'] | ."freeform-tags"."availability-domain"' "${BOOT_VOLUMES}")
  
  MYLENGTH1=$(printf "%s" " ${AD_NAME[0]}" | wc -c)
  MYLENGTH2=$(printf "%s" "$BV_AD" | wc -c)
  AD_PREFIX=${AD_NAME[0]:0:5}
  AD_REGION=${AD_NAME[0]:5:MYLENGTH1-11}
  AD_SHORT=${BV_AD:MYLENGTH2-4:4}
  BV_AD=$AD_PREFIX$AD_REGION-$AD_SHORT
  
  debug_print "oci --profile \"${PROFILE}\" bv boot-volume create --display-name \"${BV_NAME}\" --freeform-tags \"{\"unique-id\": \"$UNIQUE_ID\"}\" --boot-volume-backup-id \"${BV_ID}\" --availability-domain \"${BV_AD}\" --wait-for-state AVAILABLE"
  oci --profile "${PROFILE}" bv boot-volume create --display-name "${BV_NAME}" --freeform-tags "{\"unique-id\": \"$UNIQUE_ID\"}" --boot-volume-backup-id "${BV_ID}" --availability-domain "${BV_AD}" --wait-for-state AVAILABLE
done

# create boot volume backups from the newly created boot volumes and delete the boot volumes afterward
echo -e "$Cyan ...# create boot volume backups from the newly created boot volumes and delete the boot volumes afterward"
for i in "${!AD_NAME[@]}"; do
  debug_print "#### processing boot volumes in ${AD_NAME[$i]}"
  oci --profile "${PROFILE}" bv boot-volume list --all --compartment-id "${MY_BCKP_COMPARTMENT}" --availability-domain "${AD_NAME[$i]}"| jq --raw-output '[.data[] | select(."freeform-tags"."unique-id"=="'${UNIQUE_ID}'" and ."lifecycle-state"=="AVAILABLE")]' > "${BOOT_VOLUMES}"
  num_vols=$(( $(jq --raw-output 'length' "${BOOT_VOLUMES}") + 0 ))
  debug_print "num_vols = $num_vols"
  for ((k=0; k<$num_vols; k++)); do
    BV_ID=$(jq --raw-output '.['$k'] | ."id"' "${BOOT_VOLUMES}")
    BV_NAME=$(jq --raw-output '.['$k'] | ."display-name"' "${BOOT_VOLUMES}")
    debug_print "oci --profile \"${PROFILE}\" bv boot-volume-backup create --display-name \"${BV_NAME}\" --type full --freeform-tags \"{\"unique-id\": \"$UNIQUE_ID\"}\" --boot-volume-id \"${BV_ID}\" --wait-for-state AVAILABLE"
    debug_print "oci --profile \"${PROFILE}\" bv boot-volume delete --boot-volume-id \"${BV_ID}\" --wait-for-state TERMINATED --force"
    # create boot volume backup
    oci --profile "${PROFILE}" bv boot-volume-backup create --display-name "${BV_NAME}" --type full --freeform-tags "{\"unique-id\": \"$UNIQUE_ID\"}" --boot-volume-id "${BV_ID}" --wait-for-state AVAILABLE
    # delete boot volume
    oci --profile "${PROFILE}" bv boot-volume delete --boot-volume-id "${BV_ID}" --wait-for-state TERMINATED --force
  done
done

# move the just created boot volume backups to target compartment
# the just created boot volume backups distinguish themselves from boot volume backups
# created by savepoint.bash by having the unique-id in the freeform-tags
echo -e "$Cyan ...# move the just created boot volume backups to target compartment"
debug_print "oci --profile \"${PROFILE}\" bv boot-volume-backup list --all --compartment-id \"${MY_BCKP_COMPARTMENT}\" | jq --raw-output '[.data[] | select(.\"freeform-tags\".\"unique-id\"==\"'${UNIQUE_ID}'\" and .\"lifecycle-state\"==\"AVAILABLE\")]' > \"${BOOT_VOLUMES}\""
oci --profile "${PROFILE}" bv boot-volume-backup list --all --compartment-id "${MY_BCKP_COMPARTMENT}" | jq --raw-output '[.data[] | select(."freeform-tags"."unique-id"=="'${UNIQUE_ID}'" and ."lifecycle-state"=="AVAILABLE")]' > "${BOOT_VOLUMES}"
num_vols=$(( $(jq --raw-output 'length' "${BOOT_VOLUMES}") + 0))
for ((i=0; i<$num_vols; i++)); do
  BV_ID=$(jq --raw-output '.['$i'] | ."id"' "${BOOT_VOLUMES}")
  echo -e "$Cyan ...# oci --profile $PROFILE bv boot-volume-backup change-compartment --boot-volume-backup-id $BV_ID --compartment-id \"${COMPARTMENT_ID}\""
  oci --profile $PROFILE bv boot-volume-backup change-compartment --boot-volume-backup-id $BV_ID --compartment-id "${COMPARTMENT_ID}"
done
fi


# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# Script status output
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") : (4) CREATE Block volume backups"
echo -e "$Green ================================================================================================================================"
if [[ 1 == 1 ]]; then

################################################################################
################################################################################
# create block volumes
if [[ "false" == "${AUTOSAVEPOINT}" ]]; then
echo -e "$Cyan ...# create block volumes"
tempfile BLOCK_VOLUMES
debug_print "oci --profile \"${PROFILE}\" bv backup list --all --compartment-id \"${MY_BCKP_COMPARTMENT}\" | jq --raw-output '[.data[] | select(.\"freeform-tags\".\"Timestamp\"==\"'${SAVEPOINT}'\" and .\"lifecycle-state\"==\"AVAILABLE\")]' > \"${BLOCK_VOLUMES}\""
oci --profile "${PROFILE}" bv backup list --all --compartment-id "${MY_BCKP_COMPARTMENT}" | jq --raw-output '[.data[] | select(."freeform-tags"."Timestamp"=="'${SAVEPOINT}'" and ."lifecycle-state"=="AVAILABLE")]' > "${BLOCK_VOLUMES}"
num_vols=$(( $(jq --raw-output 'length' "${BLOCK_VOLUMES}") + 0))
debug_print "num_vols = $num_vols"
for ((i=0; i<$num_vols; i++)); do
  BV_ID=$(jq --raw-output '.['$i'] | ."id"' "${BLOCK_VOLUMES}")
  BV_NAME=$(jq --raw-output '.['$i'] | ."display-name"' "${BLOCK_VOLUMES}")
  BV_AD=$(jq --raw-output '.['$i'] | ."freeform-tags"."availability-domain"' "${BLOCK_VOLUMES}")
  
  MYLENGTH1=$(printf "%s" " ${AD_NAME[0]}" | wc -c)
  MYLENGTH2=$(printf "%s" "$BV_AD" | wc -c)
  AD_PREFIX=${AD_NAME[0]:0:5}
  AD_REGION=${AD_NAME[0]:5:MYLENGTH1-11}
  AD_SHORT=${BV_AD:MYLENGTH2-4:4}
  BV_AD=$AD_PREFIX$AD_REGION-$AD_SHORT

  debug_print "oci --profile \"${PROFILE}\" bv volume create --display-name \"${BV_NAME}\" --freeform-tags \"{\"unique-id\": \"$UNIQUE_ID\"}\" --volume-backup-id \"${BV_ID}\" --availability-domain \"${BV_AD}\" --wait-for-state AVAILABLE"
  oci --profile "${PROFILE}" bv volume create --display-name "${BV_NAME}" --freeform-tags "{\"unique-id\": \"$UNIQUE_ID\"}" --volume-backup-id "${BV_ID}" --availability-domain "${BV_AD}" --wait-for-state AVAILABLE
done
fi

################################################################################
################################################################################
# create volume backups from the newly created volumes and delete the volumes afterward
if [[ "false" == "${AUTOSAVEPOINT}" ]]; then
echo -e "$Cyan ...# create volume backups from the newly created volumes and delete the volumes afterward"
for i in "${!AD_NAME[@]}"; do
  debug_print "#### processing block volumes in ${AD_NAME[$i]}"
  oci --profile "${PROFILE}" bv volume list --all --compartment-id "${MY_BCKP_COMPARTMENT}" --availability-domain "${AD_NAME[$i]}"| jq --raw-output '[.data[] | select(."freeform-tags"."unique-id"=="'${UNIQUE_ID}'" and ."lifecycle-state"=="AVAILABLE")]' > "${BLOCK_VOLUMES}"
  num_vols=$(( $(jq --raw-output 'length' "${BLOCK_VOLUMES}") + 0 ))
  debug_print "num_vols = $num_vols"
  for ((k=0; k<$num_vols; k++)); do
    BV_ID=$(jq --raw-output '.['$k'] | ."id"' "${BLOCK_VOLUMES}")
    BV_NAME=$(jq --raw-output '.['$k'] | ."display-name"' "${BLOCK_VOLUMES}")
    debug_print "oci --profile \"${PROFILE}\" bv backup create --display-name \"${BV_NAME}\" --type full --freeform-tags \"{\"unique-id\": \"$UNIQUE_ID\"}\" --volume-id \"${BV_ID}\" --wait-for-state AVAILABLE"
    debug_print "oci --profile \"${PROFILE}\" bv volume delete --volume-id \"${BV_ID}\" --wait-for-state TERMINATED --force"
    # create block volume backup
    oci --profile "${PROFILE}" bv backup create --display-name "${BV_NAME}" --type full --freeform-tags "{\"unique-id\": \"$UNIQUE_ID\"}" --volume-id "${BV_ID}" --wait-for-state AVAILABLE
    # delete block volume
    oci --profile "${PROFILE}" bv volume delete --volume-id "${BV_ID}" --wait-for-state TERMINATED --force
  done
done
fi

if [[ "true" == "${AUTOSAVEPOINT}" ]]; then
echo -e "$Cyan ...# create volume backups from the cross region replicated block volumes"
tempfile BLOCK_VOLUMES
for i in "${!AD_NAME[@]}"; do
  debug_print "#### processing block volumes in ${AD_NAME[$i]}"
  oci --profile "${PROFILE}" bv volume list --all --compartment-id "${COMPARTMENT_ID}" --availability-domain "${AD_NAME[$i]}"| jq --raw-output '[.data[] | select(."lifecycle-state"=="AVAILABLE")]' > "${BLOCK_VOLUMES}"
  num_vols=$(( $(jq --raw-output 'length' "${BLOCK_VOLUMES}") + 0 ))
  debug_print "num_vols = $num_vols"
  for ((k=0; k<$num_vols; k++)); do
    BV_ID=$(jq --raw-output '.['$k'] | ."id"' "${BLOCK_VOLUMES}")
    BV_NAME=$(jq --raw-output '.['$k'] | ."display-name"' "${BLOCK_VOLUMES}")
	debug_print "create block volume backup ${BV_NAME:0:10}.backup"
    # create block volume backup
    oci --profile "${PROFILE}" bv backup create --display-name "${BV_NAME:0:10}.backup" --type full --freeform-tags "{\"unique-id\": \"$UNIQUE_ID\"}" --volume-id "${BV_ID}" --wait-for-state AVAILABLE
  done
done
fi

################################################################################
################################################################################
# move the just created block volume backups to target compartment
# the just created block volume backups distinguish themselves from block volume backups
# created by savepoint.bash by having the unique-id in the freeform-tags
if [[ "false" == "${AUTOSAVEPOINT}" ]]; then
echo -e "$Cyan ...# move the just created block volume backups to target compartment"
debug_print "oci --profile \"${PROFILE}\" bv backup list --all --compartment-id \"${MY_BCKP_COMPARTMENT}\" | jq --raw-output '[.data[] | select(.\"freeform-tags\".\"unique-id\"==\"'${UNIQUE_ID}'\" and .\"lifecycle-state\"==\"AVAILABLE\")]' > \"${BLOCK_VOLUMES}\""
oci --profile "${PROFILE}" bv backup list --all --compartment-id "${MY_BCKP_COMPARTMENT}" | jq --raw-output '[.data[] | select(."freeform-tags"."unique-id"=="'${UNIQUE_ID}'" and ."lifecycle-state"=="AVAILABLE")]' > "${BLOCK_VOLUMES}"
num_vols=$(( $(jq --raw-output 'length' "${BLOCK_VOLUMES}") + 0))
for ((i=0; i<$num_vols; i++)); do
  BV_ID=$(jq --raw-output '.['$i'] | ."id"' "${BLOCK_VOLUMES}")
  echo -e "$Cyan ...# oci --profile $PROFILE bv backup change-compartment --volume-backup-id $BV_ID --compartment-id \"${COMPARTMENT_ID}\""
  oci --profile $PROFILE bv backup change-compartment --volume-backup-id $BV_ID --compartment-id "${COMPARTMENT_ID}"
done
fi
################################################################################
################################################################################
fi

# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# Script status output
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") : END of script"
echo -e "$Green ================================================================================================================================"
echo -e "$Color_Off"

