#!/bin/bash
#
# (c) 2022 Heinz Mielimonka Oracle - heinz.mielimonka@oracle.com - feel free to reuse and share
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# 1. GET information about the environment 
# 2. ALL nodes in the compartment are stopped. 
#    - So also DBServ0, DBServ1, LoadTest and Bastion
#    - if further nodes have been created in the compartment, they will also be stopped.
# 3. Housekeeping
#    - delete all volume-group backups
#    - delete all Volume Backups in the current compartment
#    - delete all Boot Volume Backups in the current compartment
# 4. DELETE all block volume backups and boot volume backups
#    - NOT JUST boot volume backups of DBServ[01], LoadTest and Bastion and
#    - NOT JUST block volume backups of the asmdisk[01].* block volumes attached to DBServ[01].
#    - ALL in the current compartment
# 5. CREATE Boot volume backups of ALL boot volumes in the compartment, including those of DBServ[01], LoadTest, and Bastion
# 6. CREATE Block volume backups of ALL block volumes in the compartment, also from the block volumes asmdisk[01].*
# 7. CREATE Volume Group Backups of ALL Volume Groups (e.g. VGasmdisk0, VGasmdisk1)
# 8. MOVE all backups to DB-Test/Sicherungen/Test1BCKP
# 9. COPY all boot volume backups and Volume Group Backups Cross-Region
#10. STARTUP compute instances in compartment
# --------------------------------------------------------------------------------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# SDK and CLI Configuration File - https://docs.oracle.com/en-us/iaas/Content/API/Concepts/sdkconfig.htm
# Oracle Cloud Infrastructure CLI Command Reference - https://docs.oracle.com/iaas/tools/oci-cli/latest/oci_cli_docs/
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
PROFILE=ba
# the compartment to work with
COMPARTMENT_ID="###IT_SHOULD_BE_OBVIOUS_THIS_MUST_BE_REPLACED_BY_THE_REAL_COMPARTMENT_OCID####"
# --------------------------------------------------------------------------------------------------------------------------------------------------------------


BACKUPREGION_PARAM="--backupregion"
BACKUPREGION_ACTION=0


usage()
{
  echo -e "\nUsage :"
  echo -e "        $(basename $0)"
  echo -e "        to run backup for the environment in the given region\n"
  echo -e "        $(basename $0) ${BACKUPREGION_PARAM} <region_name>"
  echo -e "        to run backup for the environment in the local and backup region\n"  
  echo -e "        possible region names are:"
  echo -e "        +-----+-------------------+"
  echo -e "        | key | name              |"
  echo -e "        +-----+-------------------+"
  echo -e "        | AMS | eu-amsterdam-1    |"
  echo -e "        | ARN | eu-stockholm-1    |"
  echo -e "        | AUH | me-abudhabi-1     |"
  echo -e "        | BOM | ap-mumbai-1       |"
  echo -e "        | CDG | eu-paris-1        |"
  echo -e "        | CWL | uk-cardiff-1      |"
  echo -e "        | DXB | me-dubai-1        |"
  echo -e "        | FRA | eu-frankfurt-1    |"
  echo -e "        | GRU | sa-saopaulo-1     |"
  echo -e "        | HYD | ap-hyderabad-1    |"
  echo -e "        | IAD | us-ashburn-1      |"
  echo -e "        | ICN | ap-seoul-1        |"
  echo -e "        | JED | me-jeddah-1       |"
  echo -e "        | JNB | af-johannesburg-1 |"
  echo -e "        | KIX | ap-osaka-1        |"
  echo -e "        | LHR | uk-london-1       |"
  echo -e "        | LIN | eu-milan-1        |"
  echo -e "        | MEL | ap-melbourne-1    |"
  echo -e "        | MRS | eu-marseille-1    |"
  echo -e "        | MTZ | il-jerusalem-1    |"
  echo -e "        | NRT | ap-tokyo-1        |"
  echo -e "        | PHX | us-phoenix-1      |"
  echo -e "        | SCL | sa-santiago-1     |"
  echo -e "        | SIN | ap-singapore-1    |"
  echo -e "        | SJC | us-sanjose-1      |"
  echo -e "        | SYD | ap-sydney-1       |"
  echo -e "        | VCP | sa-vinhedo-1      |"
  echo -e "        | YNY | ap-chuncheon-1    |"
  echo -e "        | YUL | ca-montreal-1     |"
  echo -e "        | YYZ | ca-toronto-1      |"
  echo -e "        | ZRH | eu-zurich-1       |"
  echo -e "        +-----+-------------------+ $Color_Off"

  exit
}

case "$#" in
    0)
      BACKUPREGION_NAME=""
	  BACKUPREGION_ACTION=0
      ;;
    2)
      case "$1" in
          "${BACKUPREGION_PARAM}")
            BACKUPREGION_NAME="$2"
			BACKUPREGION_ACTION=1
            ;;
          *)
            usage;
      esac
      ;;
    *)
      usage;
esac

# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# Set Colors for output
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
Color_Off='\e[0m'       # Text Reset
Black='\e[0;30m'        # Black
Red='\e[0;31m'          # Red
Green='\e[0;32m'        # Green
Yellow='\e[0;33m'       # Yellow
Blue='\e[0;34m'         # Blue
Purple='\e[0;35m'       # Purple
Cyan='\e[0;36m'         # Cyan
White='\e[0;37m'        # White


# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# Script status output
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Color_Off"
echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") : (0) Start script savepoint.bash"
echo -e "$Green ================================================================================================================================"

# name of subcompartment for backups
BCKP_COMPARTMENT_NAME="Sicherungen"
# string will be appended to compartment name to distinguish the backup compartment and work compartment
APPENDIX="BCKP"

# Timestamp
TIMESTAMP=$(date "+%Y%m%d%H%M%S")

# generate a unique ID to tag resources created by this script
UNIQUE_ID="k4JgHrt${TIMESTAMP}"
if [ -e /dev/urandom ];then
 UNIQUE_ID=$(cat /dev/urandom|LC_CTYPE=C tr -dc "[:alnum:]"|fold -w 32|head -n 1)
fi

# array keeps a list of temporary files to cleanup
TMP_FILE_LIST=()

# do not change value if set in environment
DEBUG_PRINT="${DEBUG_PRINT:=false}"

# filename of this script
THIS_SCRIPT="$(basename ${BASH_SOURCE})"

# Do cleanup
function Cleanup() {
  for i in "${!TMP_FILE_LIST[@]}"; do
    if [ -f "${TMP_FILE_LIST[$i]}" ]; then
      debug_print "deleting ${TMP_FILE_LIST[$i]}"
      rm -f "${TMP_FILE_LIST[$i]}"
    fi
  done
}

# Do cleanup, display error message and exit
function Interrupt() {
  Cleanup
  exitcode=99
  echo -e "\nScript '${THIS_SCRIPT}' aborted by user. $Color_Off"
  exit $exitcode
}

# trap ctrl-c and call interrupt()
trap Interrupt INT
# trap exit and call cleanup()
trap Cleanup   EXIT

debug_print()
{
  if ${DEBUG_PRINT}; then
    echo -e "$Red ......# $1"
  fi
}

# call:
# tempfile my_temp_file
# to create a tempfile. The generated file is $my_temp_file
function tempfile()
{
  local __resultvar=$1
  local __tmp_file=$(mktemp -t ${THIS_SCRIPT}_tmp_file.XXXXXXXXXXX) || {
    echo -e "$Cyan ...# *** Creation of ${__tmp_file} failed $Color_Off";
    exit 1;
  }
  TMP_FILE_LIST+=("${__tmp_file}")
  if [[ "$__resultvar" ]]; then
    eval $__resultvar="'$__tmp_file'"
  else
    echo -e "$Cyan ...# $__tmp_file"
  fi
}


# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# Script status output
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") : (1) GET information about the environment"
echo -e "$Green ================================================================================================================================"

# get information about the current compartment - https://docs.oracle.com/iaas/tools/oci-cli/latest/oci_cli_docs/cmdref/iam/compartment.html
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
tempfile CURRENT_COMPARTMENT
oci --profile "${PROFILE}" iam compartment get --compartment-id "${COMPARTMENT_ID}" > "${CURRENT_COMPARTMENT}"
COMPARTMENT_NAME="$(cat "${CURRENT_COMPARTMENT}"|jq --raw-output '.data."name"')"
PARENT_COMPARTMENT_ID="$(cat "${CURRENT_COMPARTMENT}"|jq --raw-output '.data."compartment-id"')"
echo -e "$Cyan ...# current compartment name : $COMPARTMENT_NAME"
debug_print "parent_compartment_id    : $PARENT_COMPARTMENT_ID"


# get information about the parent compartment - https://docs.oracle.com/iaas/tools/oci-cli/latest/oci_cli_docs/cmdref/iam/compartment.html
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
tempfile PARENT_COMPARTMENT
oci --profile "${PROFILE}" iam compartment get --compartment-id "${PARENT_COMPARTMENT_ID}" > "${PARENT_COMPARTMENT}"
PARENT_COMPARTMENT_NAME="$(cat "${PARENT_COMPARTMENT}"|jq --raw-output '.data."name"')"
PARENT_COMPARTMENT_ID="$(cat "${PARENT_COMPARTMENT}"|jq --raw-output '.data."id"')"
echo -e "$Cyan ...# parent compartment name : $PARENT_COMPARTMENT_NAME"
debug_print "parent_compartment_id   : $PARENT_COMPARTMENT_ID"

# get the compartment used for backups - https://docs.oracle.com/iaas/tools/oci-cli/latest/oci_cli_docs/cmdref/iam/compartment.html
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
tempfile COMPARTMENT_LIST
oci --profile "${PROFILE}" iam compartment list --all --compartment-id "${PARENT_COMPARTMENT_ID}" --lifecycle-state active --name "${BCKP_COMPARTMENT_NAME}" > "${COMPARTMENT_LIST}"
num_compartments=$(( $(cat "${COMPARTMENT_LIST}"|jq --raw-output '.data | length') + 0 ))
if [[ $num_compartments != 1 ]]; then
  echo -e "$Cyan ...# Could not identify the Compartment used for backups"

  exit
fi
BCKP_COMPARTMENT_ID="$(cat "${COMPARTMENT_LIST}"|jq --raw-output '.data[0]."id"')"


# create a backup subcompartment in backup compartment for current compartment - https://docs.oracle.com/iaas/tools/oci-cli/latest/oci_cli_docs/cmdref/iam/compartment.html
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
MY_BCKP_COMPARTMENT_NAME="${COMPARTMENT_NAME}${APPENDIX}"
tempfile COMPARTMENT_LIST
oci --profile "${PROFILE}" iam compartment list --all --compartment-id "${BCKP_COMPARTMENT_ID}" --lifecycle-state active --name "${MY_BCKP_COMPARTMENT_NAME}" > "${COMPARTMENT_LIST}"
num_compartments=$(( $(cat "${COMPARTMENT_LIST}"|jq --raw-output '.data | length') + 0 ))
if [[ $num_compartments != 1 ]]; then
  echo -e "$Cyan ...# going to create \"${MY_BCKP_COMPARTMENT_NAME}\" below the \"$BCKP_COMPARTMENT_NAME\" compartment"
  oci --profile "${PROFILE}" iam compartment create --compartment-id "${BCKP_COMPARTMENT_ID}" --description "holds backups from \"${COMPARTMENT_NAME}\" compartment" --name "${MY_BCKP_COMPARTMENT_NAME}" --wait-for-state active
else
  echo -e "$Cyan ...# found \"$MY_BCKP_COMPARTMENT_NAME\" below the \"$BCKP_COMPARTMENT_NAME\" compartment"
fi
# get ocid of backup compartment
tempfile COMPARTMENT_LIST
oci --profile "${PROFILE}" iam compartment list --all --compartment-id "${BCKP_COMPARTMENT_ID}" --lifecycle-state active --name "${MY_BCKP_COMPARTMENT_NAME}" > "${COMPARTMENT_LIST}"
num_compartments=$(( $(cat "${COMPARTMENT_LIST}"|jq --raw-output '.data | length') + 0 ))
if [[ $num_compartments != 1 ]]; then
  echo -e "$Cyan \n...# there is more than one compartment \"${MY_BCKP_COMPARTMENT_NAME}\" below \"$BCKP_COMPARTMENT_NAME\"\n or it could not be created.\n\nGiving up. Exiting.\n $Color_Off"
  exit
fi
MY_BCKP_COMPARTMENT="$(cat "${COMPARTMENT_LIST}"|jq --raw-output '.data[0]."id"')"
echo -e "$Cyan ...# backup compartment name : $MY_BCKP_COMPARTMENT"

# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# Script status output
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") : (2) ALL nodes in the compartment are stopped."
echo -e "$Green ================================================================================================================================"

# shutdown compute instances in compartment - https://docs.oracle.com/iaas/tools/oci-cli/latest/oci_cli_docs/cmdref/compute.html
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
tempfile INSTANCE_LIST_JSON
oci --profile "${PROFILE}" compute instance list --all --lifecycle-state "RUNNING" --compartment-id "${COMPARTMENT_ID}" > "${INSTANCE_LIST_JSON}"
num_instances=$(( $(cat "${INSTANCE_LIST_JSON}"|jq --raw-output '.data | length') + 0 ))

for ((i=0; i<$num_instances; i++)); do
  INSTANCE_ID="$(cat "${INSTANCE_LIST_JSON}"|jq --raw-output '.data['$i']."id"')"
  AD_NAME="$(cat "${INSTANCE_LIST_JSON}"|jq --raw-output '.data['$i']."availability-domain"')"
  debug_print "instance_id = $INSTANCE_ID, ad_name = $AD_NAME"
  oci --profile $PROFILE compute instance action --action stop --instance-id $INSTANCE_ID --wait-for-state STOPPED | jq '.data|{"Server": ."display-name", "Status": ."lifecycle-state"}'
done


# start with housekeeping
# **************************************************************************************************************************************************************
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# Script status output
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") : (3) Housekeeping"
echo -e "$Green ================================================================================================================================"


# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# Script status output
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") : (4) DELETE all block volume backups and boot volume backups"
echo -e "$Green ================================================================================================================================"

# delete all volume-group backups - https://docs.oracle.com/iaas/tools/oci-cli/latest/oci_cli_docs/cmdref/bv.html
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Cyan ...# delete all volume-group backups"
tempfile BACKUP_LIST
debug_print "oci --profile \"${PROFILE}\" bv volume-group-backup list --all --compartment-id \"${COMPARTMENT_ID}\" | jq --raw-output '[.data[] | select(.\"lifecycle-state\"==\"AVAILABLE\")]' > \"${BACKUP_LIST}\""
oci --profile "${PROFILE}" bv volume-group-backup list --all --compartment-id "${COMPARTMENT_ID}" | jq --raw-output '[.data[] | select(."lifecycle-state"=="AVAILABLE")]' > "${BACKUP_LIST}"
num_vols=$(( $(jq --raw-output 'length' "${BACKUP_LIST}") + 0))
debug_print "will delete $num_vols volume group backups"
for ((i=0; i<$num_vols; i++)); do
  BV_ID=$(jq --raw-output '.['$i'] | ."id"' "${BACKUP_LIST}")
  debug_print "oci --profile \"${PROFILE}\" bv volume-group-backup delete --force --volume-group-backup-id \"${BV_ID}\" --wait-for-state TERMINATED"
  oci --profile "${PROFILE}" bv volume-group-backup delete --force --volume-group-backup-id "${BV_ID}" --wait-for-state TERMINATED
done

# delete all Volume Backups in the current compartment - https://docs.oracle.com/iaas/tools/oci-cli/latest/oci_cli_docs/cmdref/bv.html
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Cyan ...# delete all Volume Backups in the current compartment"
debug_print "oci --profile \"${PROFILE}\" bv backup list --all --compartment-id \"${COMPARTMENT_ID}\" | jq --raw-output '[.data[] | select(.\"lifecycle-state\"==\"AVAILABLE\")]' > \"${BACKUP_LIST}\""
oci --profile "${PROFILE}" bv backup list --all --compartment-id "${COMPARTMENT_ID}" | jq --raw-output '[.data[] | select(."lifecycle-state"=="AVAILABLE")]' > "${BACKUP_LIST}"
num_vols=$(( $(jq --raw-output 'length' "${BACKUP_LIST}") + 0))
debug_print "will delete $num_vols volume backups"
for ((i=0; i<$num_vols; i++)); do
  BV_ID=$(jq --raw-output '.['$i'] | ."id"' "${BACKUP_LIST}")
  debug_print "oci --profile \"${PROFILE}\" bv backup delete --force --volume-backup-id \"${BV_ID}\" --wait-for-state TERMINATED"
  oci --profile "${PROFILE}" bv backup delete --force --volume-backup-id "${BV_ID}" --wait-for-state TERMINATED
done

# delete all Boot Volume Backups in the current compartment - https://docs.oracle.com/iaas/tools/oci-cli/latest/oci_cli_docs/cmdref/bv.html
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Cyan ...# delete all Boot Volume Backups in the current compartment"
debug_print "oci --profile \"${PROFILE}\" bv boot-volume-backup list --all --compartment-id \"${COMPARTMENT_ID}\" | jq --raw-output '[.data[] | select(.\"lifecycle-state\"==\"AVAILABLE\")]' > \"${BACKUP_LIST}\""
oci --profile "${PROFILE}" bv boot-volume-backup list --all --compartment-id "${COMPARTMENT_ID}" | jq --raw-output '[.data[] | select(."lifecycle-state"=="AVAILABLE")]' > "${BACKUP_LIST}"
num_vols=$(( $(jq --raw-output 'length' "${BACKUP_LIST}") + 0))
debug_print "will delete $num_vols boot volume backups"
for ((i=0; i<$num_vols; i++)); do
  BV_ID=$(jq --raw-output '.['$i'] | ."id"' "${BACKUP_LIST}")
  debug_print "oci --profile \"${PROFILE}\" bv boot-volume-backup delete --force --boot-volume-backup-id \"${BV_ID}\" --wait-for-state TERMINATED"
  oci --profile "${PROFILE}" bv boot-volume-backup delete --force --boot-volume-backup-id "${BV_ID}" --wait-for-state TERMINATED
done
echo -e "$Cyan ...# end of housekeeping......................................................................................................................................."
# **************************************************************************************************************************************************************
# end of housekeeping


# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# Script status output
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") : (5) CREATE Boot volume backups of ALL boot volumes in the compartment, including those of DBServ[01], LoadTest, and Bastion"
echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") : (6) CREATE Block volume backups of ALL block volumes in the compartment, also from the block volumes asmdisk[01].*"
echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") : (7) CREATE Volume Group Backups of ALL Volume Groups (e.g. VGasmdisk0, VGasmdisk1)"
echo -e "$Green ================================================================================================================================"

# iterate all availability domains - https://docs.oracle.com/iaas/tools/oci-cli/latest/oci_cli_docs/cmdref/iam/availability-domain.html
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
tempfile AD_LIST_JSON
oci --profile "${PROFILE}" iam availability-domain list --all > "${AD_LIST_JSON}"
num_ads=$(( $(cat "${AD_LIST_JSON}"|jq --raw-output '.data | length') + 0 ))
debug_print "#ADs : $num_ads"
for ((i=0; i<$num_ads; i++)); do
  AD_NAME=("$(cat "${AD_LIST_JSON}"|jq --raw-output '.data['$i']."name"')")
  echo -e "$Cyan ...# iterate availability domain : ${AD_NAME}"
  # ------------------------------------------------------------------------------------------------------------------------------------------------
  # (1) create new boot volume backups in compartment and availability domain
  # ------------------------------------------------------------------------------------------------------------------------------------------------
  tempfile BV_LIST_JSON
  oci --profile "${PROFILE}" bv boot-volume list --all --compartment-id "${COMPARTMENT_ID}" --availability-domain "${AD_NAME}" > "${BV_LIST_JSON}"
  num_bvs=$(( $(cat "${BV_LIST_JSON}"|jq --raw-output '.data | length') + 0 ))
  echo -e "$Cyan ...# #boot_volumes in AD \"$AD_NAME\" = \"$num_bvs\""
  for ((k=0; k<$num_bvs; k++)); do
    BV_ID=("$(cat "${BV_LIST_JSON}"|jq --raw-output '.data['$k']."id"')")
    BV_NAME="$(cat "${BV_LIST_JSON}"|jq --raw-output '.data['$k']."display-name"')"
    BV_STATE="$(cat "${BV_LIST_JSON}"|jq --raw-output '.data['$k']."lifecycle-state"')"
    if [ "$BV_STATE" == "AVAILABLE" ]; then
      echo oci --profile "${PROFILE}" bv boot-volume-backup create --boot-volume-id "${BV_ID}" --display-name "${BV_NAME}" --type FULL --wait-for-state AVAILABLE --freeform-tags "{\"Timestamp\": \"${TIMESTAMP}\", \"availability-domain\": \"${AD_NAME}\"}"
      oci --profile "${PROFILE}" bv boot-volume-backup create --boot-volume-id "${BV_ID}" --display-name "${BV_NAME}" --type FULL --wait-for-state AVAILABLE --freeform-tags "{\"Timestamp\": \"${TIMESTAMP}\", \"availability-domain\": \"${AD_NAME}\"}"
    fi
  done
 
  # ------------------------------------------------------------------------------------------------------------------------------------------------
  # (2) create new volume group backups in compartment and availability domain
  # ------------------------------------------------------------------------------------------------------------------------------------------------
  tempfile VG_LIST_JSON
  oci --profile "${PROFILE}" bv volume-group list --all --lifecycle-state AVAILABLE --compartment-id "${COMPARTMENT_ID}" --availability-domain "${AD_NAME}" > "${VG_LIST_JSON}"
  num_vgs=$(( $(cat "${VG_LIST_JSON}"|jq --raw-output '.data | length') + 0 ))
  echo -e "$Cyan ...# #volume groups in AD \"$AD_NAME\" = \"$num_vgs\""
  for ((k=0; k<$num_vgs; k++)); do
    VG_ID=("$(cat "${VG_LIST_JSON}"|jq --raw-output '.data['$k']."id"')")
    VG_NAME="$(cat "${VG_LIST_JSON}"|jq --raw-output '.data['$k']."display-name"')"
    echo oci --profile "${PROFILE}" bv volume-group-backup create --volume-group-id "${VG_ID}" --display-name "${VG_NAME}" --type FULL --wait-for-state AVAILABLE --freeform-tags "{\"Timestamp\": \"${TIMESTAMP}\", \"availability-domain\": \"${AD_NAME}\"}"
    oci --profile "${PROFILE}" bv volume-group-backup create --volume-group-id "${VG_ID}" --display-name "${VG_NAME}" --type FULL --wait-for-state AVAILABLE --freeform-tags "{\"Timestamp\": \"${TIMESTAMP}\", \"availability-domain\": \"${AD_NAME}\"}"
  done
  
done

# display the display_name and OCID of boot volume backups
tempfile BV_BCKP_IDS
oci --profile "${PROFILE}" bv boot-volume-backup list --all --lifecycle-state "AVAILABLE" --compartment-id "${COMPARTMENT_ID}" > "${BV_BCKP_IDS}"
num_bvs=$(( $(cat "${BV_BCKP_IDS}"|jq --raw-output '.data | length') + 0 ))
echo -e "$Cyan ...# #boot_volume_backups = \"$num_bvs\""
for ((k=0; k<$num_bvs; k++)); do
  BV_BCKP_ID=("$(cat "${BV_BCKP_IDS}"|jq --raw-output '.data['$k']."id"')")
  BV_NAME="$(cat "${BV_BCKP_IDS}"|jq --raw-output '.data['$k']."display-name"')"
  echo -e "$Cyan ...# \"$BV_NAME\" = \"$BV_BCKP_ID\","
done



# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# Script status output
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") : (8) MOVE all backups to DB-Test/Sicherungen/Test1BCKP"
echo -e "$Green ================================================================================================================================"


# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# move all boot volume backups into backup compartment - https://docs.oracle.com/iaas/tools/oci-cli/latest/oci_cli_docs/cmdref/bv.html
# and copy all boot volume backups Cross-Region        - https://docs.oracle.com/iaas/tools/oci-cli/latest/oci_cli_docs/cmdref/bv/boot-volume-backup/copy.html
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
tempfile BVBCKUP_LIST_JSON
oci --profile "${PROFILE}" bv boot-volume-backup list --all --lifecycle-state "AVAILABLE" --compartment-id "${COMPARTMENT_ID}"> "${BVBCKUP_LIST_JSON}"
num_backups=$(( $(cat "${BVBCKUP_LIST_JSON}"|jq --raw-output '.data | length') + 0 ))
echo -e "$Cyan ...# #### COPY \"$num_backups\" boot volume backups with status AVAILABLE into new region $BACKUPREGION_NAME"
for ((i=0; i<$num_backups; i++)); do
  BACKUP_ID=("$(cat "${BVBCKUP_LIST_JSON}"|jq --raw-output '.data['$i']."id"')")
  echo oci --profile $PROFILE bv boot-volume-backup change-compartment --boot-volume-backup-id $BACKUP_ID --compartment-id "${MY_BCKP_COMPARTMENT}"
  oci --profile $PROFILE bv boot-volume-backup change-compartment --boot-volume-backup-id $BACKUP_ID --compartment-id "${MY_BCKP_COMPARTMENT}"
  if [[ $BACKUPREGION_ACTION == 1 ]]; then
    echo oci --profile ${PROFILE} bv boot-volume-backup copy   --boot-volume-backup-id $BACKUP_ID --destination-region $BACKUPREGION_NAME --wait-for-state AVAILABLE
    oci --profile "${PROFILE}" bv boot-volume-backup copy  --boot-volume-backup-id $BACKUP_ID --destination-region "$BACKUPREGION_NAME" --wait-for-state AVAILABLE
  fi
done


# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# move all volume group backups into backup compartment - https://docs.oracle.com/iaas/tools/oci-cli/latest/oci_cli_docs/cmdref/bv.html
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
tempfile VGBCKUP_LIST_JSON
oci --profile "${PROFILE}" bv volume-group-backup list --all --compartment-id "${COMPARTMENT_ID}"> "${VGBCKUP_LIST_JSON}"
num_backups=$(( $(cat "${VGBCKUP_LIST_JSON}"|jq --raw-output '.data | length') + 0 ))
echo -e "$Cyan ...# #### MOVE \"$num_backups\" volume group backups into new compartment ${MY_BCKP_COMPARTMENT}"
# iterate volume group backups
for ((i=0; i<$num_backups; i++)); do
  BACKUP_ID=("$(cat "${VGBCKUP_LIST_JSON}"|jq --raw-output '.data['$i']."id"')")
  BACKUP_STATE=("$(cat "${VGBCKUP_LIST_JSON}"|jq --raw-output '.data['$i']."lifecycle-state"')")
  if [ "$BACKUP_STATE" == "AVAILABLE" ]; then
    # move volume group backups into backup compartment
    echo oci --profile $PROFILE bv volume-group-backup change-compartment --volume-group-backup-id $BACKUP_ID --compartment-id "${MY_BCKP_COMPARTMENT}"
    oci --profile $PROFILE bv volume-group-backup change-compartment --volume-group-backup-id $BACKUP_ID --compartment-id "${MY_BCKP_COMPARTMENT}"
	# copy volume group backups Cross-Region
    echo oci --profile $PROFILE bv volume-group-backup change-compartment --volume-group-backup-id $BACKUP_ID --compartment-id "${MY_BCKP_COMPARTMENT}"
    oci --profile $PROFILE bv volume-group-backup change-compartment --volume-group-backup-id $BACKUP_ID --compartment-id "${MY_BCKP_COMPARTMENT}"	
    # iterate block volume backups
    num_vol_bckps=$(( $(cat "${VGBCKUP_LIST_JSON}"|jq --raw-output '.data['$i']."volume-backup-ids" | length') + 0 ))
    for ((k=0; k<$num_vol_bckps; k++)); do
      VOL_BCKP_ID="$(cat "${VGBCKUP_LIST_JSON}"|jq --raw-output '.data['$i']."volume-backup-ids"['$k']')"
      echo oci --profile "${PROFILE}" bv backup change-compartment --volume-backup-id "${VOL_BCKP_ID}"  --compartment-id "${MY_BCKP_COMPARTMENT}"
      oci --profile "${PROFILE}" bv backup change-compartment --volume-backup-id "${VOL_BCKP_ID}"  --compartment-id "${MY_BCKP_COMPARTMENT}"
    done
  fi
done



if [[ $BACKUPREGION_ACTION == 1 ]]; then
  # --------------------------------------------------------------------------------------------------------------------------------------------------------------
  # Script status output
  # --------------------------------------------------------------------------------------------------------------------------------------------------------------
  echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") : (9) COPY all boot volume backups and Volume Group Backups Cross-Region"
  echo -e "$Green ================================================================================================================================"

  # --------------------------------------------------------------------------------------------------------------------------------------------------------------
  # copy Volume Group backups Cross Region - https://docs.oracle.com/iaas/tools/oci-cli/latest/oci_cli_docs/cmdref/bv/volume-group-backup/copy.html, https://docs.oracle.com/en-us/iaas/Content/General/Concepts/regions.htm
  # --------------------------------------------------------------------------------------------------------------------------------------------------------------
  tempfile VGBCKUP_LIST_JSON
  oci --profile "${PROFILE}" bv volume-group-backup list --all --compartment-id "${MY_BCKP_COMPARTMENT}" | jq --raw-output '[.data[] | select(."lifecycle-state"=="AVAILABLE" and ."freeform-tags"."Timestamp"=="'$TIMESTAMP'")]' > "${VGBCKUP_LIST_JSON}"
  num_vgbackup=$(( $(jq --raw-output 'length' "${VGBCKUP_LIST_JSON}") + 0))
  echo -e "$Cyan ...# #volume group backups to copy cross region = \"$num_vgbackup\""
  for ((k=0; k<$num_vgbackup; k++)); do
    VG_BACKUP_ID=("$(cat "${VGBCKUP_LIST_JSON}"|jq --raw-output '.['$k'] | ."id"')")
    VG_BACKUP_NAME="$(cat "${VGBCKUP_LIST_JSON}"|jq --raw-output '.['$k'] | ."display-name"')"
    oci --profile "${PROFILE}" bv volume-group-backup copy --destination-region ${BACKUPREGION_NAME} --volume-group-backup-id ${VG_BACKUP_ID} --display-name "${VG_BACKUP_NAME}"
  done
fi


# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# Script status output
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") : (10) STARTUP compute instances in compartment"
echo -e "$Green ================================================================================================================================"

# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# STARTUP compute instances in compartment - https://docs.oracle.com/iaas/tools/oci-cli/latest/oci_cli_docs/cmdref/compute.html
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Cyan ...# startup compute instances in compartment"
num_instances=$(( $(cat "${INSTANCE_LIST_JSON}"|jq --raw-output '.data | length') + 0 ))
for ((i=0; i<$num_instances; i++)); do
  INSTANCE_ID="$(cat "${INSTANCE_LIST_JSON}"|jq --raw-output '.data['$i']."id"')"
  AD_NAME="$(cat "${INSTANCE_LIST_JSON}"|jq --raw-output '.data['$i']."availability-domain"')"
  debug_print "instance_id = $INSTANCE_ID, ad_name = $AD_NAME"
  echo oci --profile $PROFILE compute instance action --action start --instance-id $INSTANCE_ID --wait-for-state RUNNING
  oci --profile $PROFILE compute instance action --action start --instance-id $INSTANCE_ID --wait-for-state RUNNING | jq '.data|{"Server": ."display-name", "Status": ."lifecycle-state"}'
done

# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# Script status output
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") : END of script"
echo -e "$Green ================================================================================================================================"
echo -e "$Color_Off"

