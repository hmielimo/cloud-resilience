#!/bin/bash
#
# (c) 2022 Heinz Mielimonka Oracle - heinz.mielimonka@oracle.com - feel free to reuse and share
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# automate OCI Block Storage crossregion volume group replication based on 
# Cloud Resilience Example (https://gitlab.com/hmielimo/cloud-resilience/-/blob/main/doc/cloud.resilience/README.md#cloud-resilience-example)
# --------------------------------------------------------------------------------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# SDK and CLI Configuration File - https://docs.oracle.com/en-us/iaas/Content/API/Concepts/sdkconfig.htm
# Oracle Cloud Infrastructure CLI Command Reference - https://docs.oracle.com/iaas/tools/oci-cli/latest/oci_cli_docs/
# OCI Block Storage crossregion volume group replication - https://blogs.oracle.com/cloud-infrastructure/post/announcing-oci-block-storage-cross-region-volume-group-replication
# --------------------------------------------------------------------------------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# START needed functions and variables
# --------------------------------------------------------------------------------------------------------------------------------------------------------------

DEBUG_PRINT="${DEBUG_PRINT:=false}"
PROFILE=ba
COMPARTMENT_PARAM="--compartmentOCID"
CROSSREGION_ACTION="--crossregionACTION"
COMPARTMENT_ACTION=0
REPLICA_PROFILE=london
BACKUPREGION_NAME="uk-london-1"


# Set Colors for output
Color_Off='\e[0m'       # Text Reset
Black='\e[0;30m'        # Black
Red='\e[0;31m'          # Red
Green='\e[0;32m'        # Green
Yellow='\e[0;33m'       # Yellow
Blue='\e[0;34m'         # Blue
Purple='\e[0;35m'       # Purple
Cyan='\e[0;36m'         # Cyan
White='\e[0;37m'        # White

# generate a unique ID to tag resources created by this script
UNIQUE_ID="k4JgHrt${TIMESTAMP}"
if [ -e /dev/urandom ];then
 UNIQUE_ID=$(cat /dev/urandom|LC_CTYPE=C tr -dc "[:alnum:]"|fold -w 32|head -n 1)
fi

# array keeps a list of temporary files to cleanup
TMP_FILE_LIST=()

# filename of this script
THIS_SCRIPT="$(basename ${BASH_SOURCE})"

# Do cleanup
function Cleanup() {
  for i in "${!TMP_FILE_LIST[@]}"; do
    if [ -f "${TMP_FILE_LIST[$i]}" ]; then
      debug_print "deleting ${TMP_FILE_LIST[$i]}"
      rm -f "${TMP_FILE_LIST[$i]}"
    fi
  done
}

# Do cleanup, display error message and exit
function Interrupt() {
  Cleanup
  exitcode=99
  echo -e "\nScript '${THIS_SCRIPT}' aborted by user."
  exit $exitcode
}

# trap ctrl-c and call interrupt()
trap Interrupt INT
# trap exit and call cleanup()
trap Cleanup   EXIT

# call:
# tempfile my_temp_file
# to create a tempfile. The generated file is $my_temp_file
function tempfile()
{
  local __resultvar=$1
  local __tmp_file=$(mktemp -t ${THIS_SCRIPT}_tmp_file.XXXXXXXXXXX) || {
    echo -e "$Cyan ...# *** Creation of ${__tmp_file} failed";
    exit 1;
  }
  TMP_FILE_LIST+=("${__tmp_file}")
  if [[ "$__resultvar" ]]; then
    eval $__resultvar="'$__tmp_file'"
  else
    echo -e "$Cyan ...# $__tmp_file"
  fi
}


debug_print()
{
  if ${DEBUG_PRINT}; then
    echo -e "$Red ......# $1 $Color_Off"
  fi
}

usage()
{
  echo -e "$Color_Off"
  echo -e "\nUsage :"
  echo -e "        $(basename $0)"
  echo -e "        to run OCI Block Storage crossregion volume group replication\n"
  echo -e "        $(basename $0) ${COMPARTMENT_PARAM} <compartment_OCID> ${CROSSREGION_ACTION} <ON | OFF>"

  exit
}

# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# END needed functions and variables
# --------------------------------------------------------------------------------------------------------------------------------------------------------------


case "$#" in
    4)
       if [[ "$1" == "${COMPARTMENT_PARAM}" ]]
	   then
            COMPARTMENT_PARAM="$2"
			COMPARTMENT_ID="${COMPARTMENT_PARAM}"
			COMPARTMENT_ACTION=1
			debug_print "compartment_id        : $COMPARTMENT_PARAM"
	   fi
       if [[ "$3" == "${CROSSREGION_ACTION}" ]]
	   then
		    CROSSREGION_ACTION="$4"
			if [[ $COMPARTMENT_ACTION != 1 ]]
			then
			  usage;
			fi
			if [[ $CROSSREGION_ACTION == "YES" || $CROSSREGION_ACTION == "NO" ]]
			then
			  debug_print "crossregion action    : $CROSSREGION_ACTION"
            else
			  usage;
			fi
	   fi
       ;;
    *)
      usage;
esac





# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# Script status output
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Color_Off"
echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") :  Start script crossregionVGR.bash"
echo -e "$Green ================================================================================================================================"


for AD_NAME in "fyxu:EU-FRANKFURT-1-AD-1" "fyxu:EU-FRANKFURT-1-AD-2" "fyxu:EU-FRANKFURT-1-AD-3"
do

MYLENGTH=$(printf "%s" "$AD_NAME" | wc -c)
let START_POS=$MYLENGTH-4
AD_PREFIX=${AD_NAME:0:5}
AD_SHORT=${AD_NAME:$START_POS:4}
REPLICA_AD_NAME=$AD_PREFIX$BACKUPREGION_NAME-$AD_SHORT


if [[ $CROSSREGION_ACTION == "YES" ]]
then 
  echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") :  ACTIVATE OCI Block Storage crossregion volume group replication in $AD_NAME"
  echo -e "$Green ================================================================================================================================"
  tempfile MY_LIST
  oci --profile "${REPLICA_PROFILE}" bv volume-group-replica list --compartment-id "${COMPARTMENT_ID}" --availability-domain "${REPLICA_AD_NAME}" --lifecycle-state "AVAILABLE" > "${MY_LIST}"
  num_vgs=$(( $(cat "${MY_LIST}"|jq --raw-output '.data | length') + 0 ))
  if [[ $num_vgs == 0 ]]; then 
    #
	# IF num_vg=0 => until now there is no asynchronous volume group replication to another region enabled, do it now
    #
	echo -e "$Cyan ...# CREATE asynchronous volume group replication"
    oci --profile "${PROFILE}" bv volume-group list --all --lifecycle-state AVAILABLE --compartment-id "${COMPARTMENT_ID}" --availability-domain "${AD_NAME}" > "${MY_LIST}"
    num_vgs=$(( $(cat "${MY_LIST}"|jq --raw-output '.data | length') + 0 ))
    debug_print "#volume groups in AD $AD_NAME = $num_vgs"
    for ((k=0; k<$num_vgs; k++)); do
      VG_ID=("$(cat "${MY_LIST}"|jq --raw-output '.data['$k']."id"')")
      VG_NAME="$(cat "${MY_LIST}"|jq --raw-output '.data['$k']."display-name"')"
	  debug_print "#volume-group update $VG_NAME"
	  debug_print "#availabilityDomain $REPLICA_AD_NAME"
      oci --profile "${PROFILE}" bv volume-group update --volume-group-id $VG_ID --volume-group-replicas "[{\"displayName\": \"${VG_NAME}\", \"availabilityDomain\": \"${REPLICA_AD_NAME}\"}]" --force
    done
	#
	# Activating the replica volume group in the target region
    #
	if [[ $num_vgs > 0 ]]; then 
	  echo -e "$Cyan ...# ACTIVATE replica volume group in the target region"
	  for ((k=0; k<30; k++)); do
	    oci --profile "${REPLICA_PROFILE}" bv volume-group-replica list --compartment-id "${COMPARTMENT_ID}" --availability-domain "${REPLICA_AD_NAME}" --lifecycle-state "AVAILABLE" > "${MY_LIST}"
        num_rvgs=$(( $(cat "${MY_LIST}"|jq --raw-output '.data | length') + 0 ))
	    if [[ $num_rvgs == 0 ]]; then 
	  	  sleep 10
	    else
		  k=30
	    fi
	    echo -e "$Cyan ...# $k try to find volume-group-replica in lifecycle-state AVAILABLE, wait 10 seconds and try again"
      done
	  debug_print "#replica volume groups in AD $AD_NAME = $num_rvgs"
      for ((k=0; k<$num_rvgs; k++)); do
        RVG_ID=("$(cat "${MY_LIST}"|jq --raw-output '.data['$k']."id"')")
        RVG_NAME="$(cat "${MY_LIST}"|jq --raw-output '.data['$k']."display-name"')"
	    debug_print "#replica volume-group ID $RVG_ID"
	    debug_print "#replica volume-group NAMe $RVG_NAME"
	    debug_print "#replica availabilityDomain $REPLICA_AD_NAME"
        oci --profile "${REPLICA_PROFILE}" bv volume-group create --source-details "{\"type\": \"volumeGroupReplicaId\", \"volumeGroupReplicaId\": \"${RVG_ID}\"}" --display-name "${RVG_NAME}" --compartment-id "${COMPARTMENT_ID}" --availability-domain "${REPLICA_AD_NAME}"
      done
	fi
  fi
fi

if [[ $CROSSREGION_ACTION == "NO" ]]
then 
  echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") :  DE-ACTIVATE OCI Block Storage crossregion volume group replication in $AD_NAME"
  echo -e "$Green ================================================================================================================================"
  tempfile MY_LIST
  oci --profile "${PROFILE}" bv volume-group list --all --lifecycle-state AVAILABLE --compartment-id "${COMPARTMENT_ID}" --availability-domain "$AD_NAME" > "${MY_LIST}"
  num_vgs=$(( $(cat "${MY_LIST}"|jq --raw-output '.data | length') + 0 ))
  echo -e "$Cyan ...# found $num_vgs volume groups in $AD_NAME"
  echo -e "$Cyan ...# DE-ACTIVATE Cross Region Replication in volume groups"
  for ((i=0; i<$num_vgs; i++)); do
    VG_ID="$(cat "${MY_LIST}"|jq --raw-output '.data['$i']."id"')"
    VG_NAME="$(cat "${MY_LIST}"|jq --raw-output '.data['$i']."display-name"')"
    debug_print "update volume group $VG_ID // $VG_NAME"
    oci --profile "${PROFILE}" bv volume-group update --volume-group-id $VG_ID --volume-group-replicas "[]" --force
  done

  oci --profile "${PROFILE}" bv volume list --all --lifecycle-state AVAILABLE --compartment-id "${COMPARTMENT_ID}" --availability-domain "$AD_NAME" > "${MY_LIST}"
  num_vgs=$(( $(cat "${MY_LIST}"|jq --raw-output '.data | length') + 0 ))
  echo -e "$Cyan ...# found $num_vgs volumes in $AD_NAME"
  echo -e "$Cyan ...# DE-ACTIVATE Cross Region Replication in volumes"
  for ((i=0; i<$num_vgs; i++)); do
    V_ID="$(cat "${MY_LIST}"|jq --raw-output '.data['$i']."id"')"
    V_NAME="$(cat "${MY_LIST}"|jq --raw-output '.data['$i']."display-name"')"
    debug_print "update volume group $V_ID // $V_NAME"
    oci --profile "${PROFILE}" bv volume update --volume-id $V_ID --block-volume-replicas "[]" --force
  done
  
  oci --profile "${REPLICA_PROFILE}" bv volume-group list --all --lifecycle-state AVAILABLE --compartment-id "${COMPARTMENT_ID}" --availability-domain "$REPLICA_AD_NAME" > "${MY_LIST}"
  num_vgs=$(( $(cat "${MY_LIST}"|jq --raw-output '.data | length') + 0 ))
  echo -e "$Cyan ...# found $num_vgs volume groups in $REPLICA_AD_NAME"
  echo -e "$Cyan ...# DELETE Volume Group"
  for ((i=0; i<$num_vgs; i++)); do
    VG_ID="$(cat "${MY_LIST}"|jq --raw-output '.data['$i']."id"')"
    VG_NAME="$(cat "${MY_LIST}"|jq --raw-output '.data['$i']."display-name"')"
    debug_print "DELETE Volume Group $VG_ID // $VG_NAME"
    oci --profile "${REPLICA_PROFILE}" bv volume-group delete --volume-group-id $VG_ID --force
  done

fi

done

# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# Script status output
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
echo -e "$Green $(date "+%d.%m.%Y %H:%M:%S") : END of script"
echo -e "$Green ================================================================================================================================"
echo -e "$Color_Off"
