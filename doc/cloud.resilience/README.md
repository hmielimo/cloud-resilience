# Cloud Resilience

[back to Next-Generation Cloud][home]

## Table of Contents

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:0 orderedList:0 -->

- [Cloud Resilience](#cloud-resilience)
  - [Table of Contents](#table-of-contents)
  - [Introduction](#introduction)
  - [Find the optimal balance for your requirements](#find-the-optimal-balance-for-your-requirements)
  - [Oracle Architectural Best Practices](#oracle-architectural-best-practices)
  - [Cloud Resilience Example](#cloud-resilience-example)
    - [Cloud Resilience Example - Workflow](#cloud-resilience-example-workflow)
    - [Cloud Resilience Example - Initial Setup](#cloud-resilience-example-initial-setup)
      - [Preperation](#preperation)
      - [create compartment structure](#create-compartment-structure)
      - [create the operation host](#create-the-operation-host)
      - [set up and configure the operation host](#set-up-and-configure-the-operation-host)
      - [Details of your Cloud Resilience Example environment](#details-of-your-cloud-resilience-example-environment)
    - [Cloud Resilience Example - Initial run](#cloud-resilience-example-initial-run)
      - [connect working compartment to operation host](#connect-working-compartment-to-operation-host)
      - [Create the new environment from scratch](#create-the-new-environment-from-scratch)
      - [local and remote backup new environment](#local-and-remote-backup-new-environment)
      - [restore new environment](#restore-new-environment)
      - [run your restore in the needed region](#run-your-restore-in-the-needed-region)
      - [OCI Block Storage crossregion volume group replication](#oci-block-storage-crossregion-volume-group-replication)
      - [clean up](#clean-up)

<!-- /TOC -->

## Introduction

Here we focus on Cloud Resilience or "Keep your systems running". [Martin Klier][CR1_MK] an Oracle ACE Director philosophizes about this in his [DOAG Datenbank Kolumne: Cloud-Multis und Multi-Cloud][CR1_MKK] based on his own experiences.

First it's worth to listen to Pradeep Vincent's (SVP, Chief Technical Architect OCI) famous talk ([Resiliency in the cloud: Separating myth from fact](https://www.youtube.com/watch?v=CjvOxlKbhcg&list=PLcFwxJMrxygBVqeRxAMPRtz1Afz3fj7L9&index=27)) on CloudWorld 2022 regarding Resiliency in the cloud. We challenge the industry assumption that a 3-Data-Center model provide maximum resiliency. We believe that maximum resiliency requires Data-Center isolation AND Maximum Software & Deployment Isolation. In our [Resilience and Continuous Availability of Oracle Cloud Infrastructure Services and Platform FAQ](https://www.oracle.com/cloud/iaas/faq.html) you find more detailed answers.

Second lets align terms across Oracle, Amazon, Google and Microsoft.

| Oracle (Oracle Cloud Infrastructure OCI)                                                                                   | Amazon (AWS)                                                                            | Google (Cloud)                                                                               | Microsoft (Azure)                                                                                    |
|----------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------|
| [Fault Domain](https://docs.oracle.com/en-us/iaas/Content/GSG/Concepts/concepts-physical.htm#concepts-physical)            | not available                                                                           | [Virtualized zones](https://cloud.google.com/compute/docs/regions-zones/zone-virtualization) | not available                                                                                        |
| [Avilability Domain](https://docs.oracle.com/en-us/iaas/Content/GSG/Concepts/concepts-physical.htm#concepts-physical)      | [Availability Zone](https://aws.amazon.com/about-aws/global-infrastructure/regions_az/) | [Zone](https://cloud.google.com/compute/docs/regions-zones)                                  | [Availability Zone](https://learn.microsoft.com/en-us/azure/reliability/availability-zones-overview) |
| [Region](https://docs.oracle.com/en-us/iaas/Content/GSG/Concepts/concepts-physical.htm#concepts-physical)                  | [Region](https://aws.amazon.com/about-aws/global-infrastructure/regions_az/)            | [Region](https://cloud.google.com/compute/docs/regions-zones)                                | [Region](https://learn.microsoft.com/en-us/azure/reliability/availability-zones-overview)            |

To provide you some hands on guidance we focus on a real world example based on which you are able to gain insides for your individual cloud resilience implementation. Please keep in mind, that Oracle provide you a lot of Cloud Resilience e.g.

- Services - e.g. [Autonomous Database](https://www.oracle.com/autonomous-database/), [Autonomous Data Guard](https://blogs.oracle.com/maa/post/autonomous-data-guard-disaster-recovery-protection-with-a-couple-clicks-in-the-cloud), [Autonomous Linux](https://www.oracle.com/linux/autonomous-linux/), [Migration & Disaster Recovery (is coming soon)](https://pdpm.oracle.com/pls/apex/f?p=102:100)
- Functionality - e.g. [Volume Group Cross Region Replication][CR1_wp8]

nevertheless we focus here on your individual implementations, fully cloud provider agnostic based on [Infrastructure as Code (IaC)](https://en.wikipedia.org/wiki/Infrastructure_as_code). Our [Cloud Resilience Example](#cloud-resilience-example) is based on [Terraform](https://en.wikipedia.org/wiki/Terraform_(software)) and [Oracle Cloud Infrastructure (OCI)](https://www.oracle.com/cloud/).

You are aware, that Services incl. Resilience features like [Autonomous Data Guard](https://docs.oracle.com/en-us/iaas/autonomous-database-shared/doc/autonomous-data-guard.html) deliver huge business benefits without burdening you with technical details or risks. This route may not be available to you. Be it because you have to run your own and/or outdated applications or for other reasons. In such cases, the extensive OCI range of resilience functionality comes in handy. Exactly such scenarios we would like to present here for you in detail.

[Backup and Disaster Recovery solutions on Oracle Cloud Infrastructure](https://www.oracle.com/cloud/backup-and-disaster-recovery/) is also a valuable place to be. Here you find solution to protect your data and applications on-premises and in the cloud with a broad range of flexible business continuity solutions. These solutions are based on our proven cloud platform, offered across a global, secure network of cloud regions. From high-bandwidth file synchronization to zero data loss database protection and detailed database failover options, Oracle Cloud offers scalable, reliable, secure, and extremely cost-effective resiliency and [disaster recovery](https://www.oracle.com/cloud/backup-and-disaster-recovery/what-is-disaster-recovery/) (DR) solutions.

We’re excited to [announce the general availability of Oracle Cloud Infrastructure (OCI) Full Stack Disaster Recovery](https://blogs.oracle.com/cloud-infrastructure/post/fsdr-launch). This service is the first true disaster recovery-as-a-service (DRaaS) solution for OCI that provides comprehensive disaster recovery management for an entire application stack with a single click.
Full Stack Disaster Recovery is a flexible, highly extensible, and scalable DRaaS solution for cross-regional disaster recovery in OCI. The service is ideal for moving workloads between OCI regions for maintenance, rolling upgrades, blue and green deployments, and any number of other uses beyond disaster recovery.

[Full Stack Disaster Recovery Service](https://oracle.com/cloud/full-stack-disaster-recovery/) has the built-in intelligence needed to automatically create basic disaster recovery plans based solely on the virtual machines, storage volume groups and databases you’ve included in a DRPG. This feature is one of many that dramatically reduce the amount of effort and time spent configuring disaster recovery.

Accelerate your implementation of business continuity with a higher degree of reliability at a lower cost to implement, maintain, and use.


[Oracle LiveLabs](https://apexapps.oracle.com/pls/apex/dbpm/r/livelabs) gives you access to Oracle's tools and technologies to run a wide variety of labs and workshops. Experience Oracle's best technology, live! (e.g. [LiveLaps in Focus Areas of Disaster Recovery, High Availability, Resilience](https://apexapps.oracle.com/pls/apex/dbpm/r/livelabs/livelabs-workshop-cards?c=y&p100_focus_area=38:39:41))

In our [Oracle Solutions Hub](https://www.oracle.com/cloud/solutions/) both Data Protection Solutions and Backup and Disaster Recovery solutions on Oracle Cloud Infrastructure help you solving your next business need by exploring and selecting from proven cloud solutions, ready-to-deploy reference architectures, and specific use case designs.

- [Data Protection Solutions](https://www.oracle.com/cloud/data-protection/)
  - Intellectual property, financial transactions, and business data are among the most valuable assets of any organization. Businesses can now protect their mission-critical data and applications with confidence by adopting Oracle Cloud Infrastructure storage solutions as migration and backup destinations, reducing cost and complexity while maintaining operational consistency.
- [Backup and Disaster Recovery solutions on Oracle Cloud Infrastructure](https://www.oracle.com/cloud/backup-and-disaster-recovery/)
  - Protect your data and applications on-premises and in the cloud with a broad range of flexible business continuity solutions. These solutions are based on our proven cloud platform, offered across a global, secure network of cloud regions. From high-bandwidth file synchronization to zero data loss database protection and detailed database failover options, Oracle Cloud offers scalable, reliable, secure, and extremely cost-effective resiliency and [disaster recovery](https://www.oracle.com/cloud/backup-and-disaster-recovery/what-is-disaster-recovery/) (DR) solutions.
  
  
**Remark: [The Home Region][CR1_homeregion]**

When you sign up for Oracle Cloud Infrastructure, Oracle creates a tenancy for you in one region. This is your home region. Your home region is where your IAM resources are defined. When you subscribe to another regions, your IAM resources are available in the new regions, however, the master definitions reside in your home region and can only be **changed** there.

Resources that you can create and update only in the home region are:
- Users
- Groups
- Policies
- Compartments
- Dynamic groups
- Federation resources

In addition the Cost Management - [Cost Analysis (dashboard)][CR1_costanalyse] is only availaible in the Home Region too. Think about this if you choose your Home Region (during contract negotiation). You might choose a multi Availability Domain Home Region for better Cloud Resilience.

## Find the optimal balance for your requirements

[<img alt="Cost Perspective Overview" src="doc/images/cost.perspective.overview.jpg" title="Cost Perspective Overview" width="100%">][CR1_costexample]


| Criteria   | [Block Volume Backup](https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/blockvolumebackups.htm)                           | [Block Volume Backup Copy](https://docs.oracle.com/en-us/iaas/Content/Block/Tasks/copyingvolumebackupcrossregion.htm)                     | [Cross Region Replication](https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/volumereplication.htm)                  |
|------------|----------------------------------|----------------------------------|-------------------------------------------|
| Protection | own mistake or Volume/AD failure | + Region failure                 | Region/AD failure                         |
| RPO        | hours to days                    | hours to days                    | single digit minutes                      |
| RTO        | higher latency while restoring   | higher latency while restoring   | lower latency, hydrating from NVMe drives |
| Cost       | low                              | medium (*+data transfer*)        | higher, more frequent data transfer       |


## Oracle Architectural Best Practices


| outage scenario              | solution                                      | comment                                                     |
|------------------------------|-----------------------------------------------|-------------------------------------------------------------|
| Region offline               | apply [Architectural Best Practices][CR1_wp7] e.g. [Deploy a highly available bare metal database][CR1_wp9] or [Disaster Recovery: Pilot Light - App & DB][CR1_wp12] |  make use of [Volume Group Cross Region Replication][CR1_wp8]   |
| Availability Domain offline  | apply [Architectural Best Practices][CR1_wp7] e.g. [Deploy Apache Tomcat connected to MySQL Database Service][CR1_wp10] but instead of using different Fault Domains, use different Availability Domains. |     |
| Server offline               | apply [Architectural Best Practices][CR1_wp7] e.g. [Deploy a highly available Microsoft SQL Server database][CR1_wp11] |   |

Here you find a global Multi Region Example (Source: [Disaster Recovery: Pilot Light - App & DB][CR1_wp12]). We focus in our [Cloud Resilience Example](#cloud-resilience-example) on the Scripted Replication of Boot and Block volumes.

[<img alt="Global Multi Region Example" src="doc/images/sample.architecture.final.solution.png" title="Global Multi Region Example" width="80%">]

## Cloud Resilience Example

[<img alt="Cloud Resilience Example" src="doc/images/cloud.resilience.example.jpg" title="Cloud Resilience Example" width="80%">][CR1_example]

Our Cloud Resilience Example have several subsystems

- load generator
- application server
- bastian host

each subsystem is one or more times needed. We will show in our Cloud Resilience Example how to deal with different outage scenarios, like e.g.

- Fault Domain or Server offline
- Availability Domain offline
- Region offline

As described in [Architectural Best Practices][CR1_wp7] Implement DR you need solutions for e.g.

- Backup your data
- Choose replication methods for your application data

both topics are covered here in the Cloud Resilience Example. In the technical white paper [Deploying Custom Operating System Images on Oracle Cloud Infrastructure][CR1_wp1], the functional description that is used here is presented in Use Case 1: Custom Images:
-	Step 1: Launch a Bare Metal Instance
-	Step 2: Connect to the Instance
-	Step 3: Install Software on the Instance
-	Step 4: Create a Custom Image of the Instance
-	Step 5: Launch an Instance by Using the Custom Image
-	Step 6: Connect to the Custom Image Instance
-	Step 7: Verify That the Software Is Installed on the New Instance

The respective custom image can be easily generated (steps 1 to 4) and reliably restored (step 5). Further security and flexibility can be achieved by separating the boot and data volumes. Boot volumes can be backed up ([Cloning a Boot Volume][CR1_wp2]) and restored ([Recovering a Corrupted Boot Volume for Linux-Based Instances][CR1_wp3]). The data volumes can also be backed up ([backing up a volume][CR1_wp4] or [cloning a volume][CR1_wp5]) and restored (simply using the backed up volume). The necessary backup or restore time depends essentially on the method selected (back up or clone) and can be as little as seconds if necessary.
Furthermore, volume groups (for details see [Introducing Volume Groups: Enabling Coordinated Backups and Clones for Application Protection and Lifecycle Management][CR1_wp6]) can be used to logically combine multiple volumes and thus ensure consistent backups or restores across all three servers (2 application servers plus a load generator). be reached.

### Cloud Resilience Example - Workflow

- Set up your Cloud Resilience environment (see [Cloud Resilience Example - Initial Setup](#cloud-resilience-example-initial-setup))
- Test your Cloud Resilience environment
 - Create the new environment from scratch in [production region]
 - Backup the environment in [production region] and [backup region]
 - Destroy the environment in [production region]
 - Restore the environment in [production region]
 - Destroy the environment in [production region]
 - Restore the environment in [backup region]
- Adapt the environment to your needs
- Automate the environment


 [<img alt="Cloud Resilience Example - Process workflow" src="doc/images/cloud.resilience.example.process.workflow.jpg" title="Cloud Resilience Example - Process workflow" width="80%">][CR1_exampleprocess]


### Cloud Resilience Example - Initial Setup


#### Preperation

Please prepare the following details:

| what                         | this is how it is shown in the text   | comments                              |
|------------------------------|---------------------------------------|---------------------------------------|
| Cloud User                   | [clouduser]                           |                                       |
| Cloud User OCID              | [clouduserOCID]                       |                                       |
| Tenancy OCID                 | [tenancyOCID]                         |                                       |
| OS User                      | [OSuser]                              | opc for Oracle Entreprise Linux       |
| [SSH Key](https://www.oracle.com/webfolder/technetwork/tutorials/obe/cloud/compute-iaas/generating_ssh_key/generate_ssh_key.html)         | [opcPRIVATEkey], [opcPUBLICkey]       | You can use the [SSH Key Generator][CR1-keygen] to create all needed keys. |
| [your Cloud Resilience Example Compartment](https://docs.oracle.com/en-us/iaas/Content/Identity/Tasks/managingcompartments.htm)      | [DB-Test]                             |                                       |
| [API Key](https://docs.oracle.com/en-us/iaas/Content/API/Concepts/apisigningkey.htm#Required_Keys_and_OCIDs) of your [clouduser]               | [APIkey]                              |                                       |
| [Fingerprint](https://docs.oracle.com/en-us/iaas/Content/API/Concepts/apisigningkey.htm#four) of your [APIKey]                       | [fingerprint]                         |                                       |
| New Compartment DB-Test OCID (available after next step)                | [compartmentOCID]                         |                                       |

Please be aware, that this Cloud Resilience Example contain two major elements (terraform and bash scripts). Both elements add to the this Cloud Resilience Example and are aligned.

#### create compartment structure

Create the following new and empty compartment structure in your tenancy.

 - [DB-Test]
 - [DB-Test]/Operations
 - [DB-Test]/Sicherungen
 - [DB-Test]/Test1

 [<img alt="Cloud Resilience Example - Overview" src="doc/images/cloud.resilience.example.overview.jpg" title="Cloud Resilience Example - Overview" width="80%">][CR1_exampleoverview]


#### create the operation host

Create a new Instance in the compartment [DB-Test]/Operations (e.g. OEL 7.9)

#### set up and configure the operation host

ssh to your new operation host [operation]

- bring your OS up to date
   ~~~
   sudo yum update -y
   ~~~
- setup Oracle Cloud Infrastructure CLI ([Command Line Interface)](https://docs.oracle.com/en-us/iaas/Content/API/Concepts/cliconcepts.htm)
    ~~~
    # setup cli
    # --------------------------------------------------------------
    # Quickstart Installing the CLI: https://docs.oracle.com/en-us/iaas/Content/API/SDKDocs/cliinstall.htm
    # here for Oracle Linux 7
    sudo yum install -y python36-oci-cli
    bash -c "$(curl -L https://raw.githubusercontent.com/oracle/oci-cli/master/scripts/install/install.sh)"
    oci --version
    #
    # config cli
    # --------------------------------------------------------------
    oci setup config
    # Cloud User: OCID [clouduserOCID]
    # Tenancy OCID: [tenancyOCID]
    # 16: eu-frankfurt-1
    #
    # update private key with your [opcPRIVATEkey]
    # --------------------------------------------------------------
    vi ~/.oci/oci_api_key.pem
    #
    # update public key with your [opcPUBLICkey]
    # --------------------------------------------------------------
    vi ~/.oci/oci_api_key_public.pem
    # add new API Key to the Cloud User and use this public key
    #
    # add ba to .oci/config (copy all of [DEFAULT] as [ba] and change appropriate)
    # --------------------------------------------------------------
    vi ~/.oci/config
    # example
    # --------------------------------------------------------------
    # [ba]
    # user=[clouduserOCID]
    # fingerprint=[fingerprint]
    # key_file=/home/opc/.oci/oci_api_key.pem
    # tenancy=[tenancyOCID]
    # region=eu-frankfurt-1
    #
    # check if cli works (maybe you have to wait 1 minute)
    # --------------------------------------------------------------
    oci --profile ba iam compartment list --query "data[*]".{'compartment:name'}  --output table
    oci --profile ba iam region      list --query "data[*]".{'key:key,name:name'} --output table
    ~~~
- setup Terraform
    ~~~
    sudo yum install -y yum-utils
    sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo
    sudo yum -y install terraform
    terraform -version
    ~~~
- clone GitLab Repository to operation Host
    ~~~
    sudo yum install -y git
    rm -rf ~/DB-Test
    mkdir ~/DB-Test
    git clone https://gitlab.com/hmielimo/cloud-resilience.git ~/DB-Test
    ls -lah ~/DB-Test
    ls -lah ~/DB-Test/doc/cloud.resilience/.TerraformTemplate/test-env/
    ~~~
- update local GitLab Repository on operation Host
    ~~~
    #
    # move Cloud Resilience Example environment
    # --------------------------------------------------------------
    cp -rf ~/DB-Test/doc/cloud.resilience/.TerraformTemplate ~/.
    cp ~/DB-Test/doc/cloud.resilience/bin/provision_new_compartments.bash ~/bin/provision_new_compartments.bash
    chmod 755 ~/bin/provision_new_compartments.bash
    mkdir ~/terraform
    #
    # update terraform.tfvars (please ONLY update "please_update" values)
    # --------------------------------------------------------------
    vi ~/.TerraformTemplate/test-env/terraform.tfvars
    # tenancy_ocid     = "please_update"
    # user_ocid        = "please_update"
    # fingerprint      = "please_update"
    #
    # backup and (only is you re-sync with the GitLap repo) restore terraform.tfvars
    # --------------------------------------------------------------
    cp ~/.TerraformTemplate/test-env/terraform.tfvars ~/terraform.tfvars.backup
    cp ~/terraform.tfvars.backup ~/.TerraformTemplate/test-env/terraform.tfvars
    #
    # update compartment OCID in provision_new_compartments.bash (please ONLY update "please_update" value)
    # --------------------------------------------------------------
    vi ~/bin/provision_new_compartments.bash
    # Compartment DB-Test
    COMPARTMENT_ID="please_update"
    #
    # if you like to see debug outputs change false to true
    # --------------------------------------------------------------
    DEBUG_PRINT="${DEBUG_PRINT:=false}"
    ~~~

#### Details of your Cloud Resilience Example environment

This section is only for your information. You find details regarding Connectivity, Operating System, Sizing (e.g. Compute, Storage) in general (/home/opc/.TerraformTemplate/test-env/...) and Compartment specific (/home/opc/terraform/[compartment]/...). Feel free to adapt to your needs or just go with the given defaults.

- **Connectivity details** (adaptation to own needs possible)
  - Terraform's Oracle Cloud credentials are located in ~/terraform/**[compartment]**/terraform.tfvars and came from ~/.TerraformTemplate/test-env/terraform.tfvars (copied by provision_new_compartments.bash)
  - The OCI access data of the script savepoint.bash and restore.bash to the Oracle Cloud are in the file ~/.oci/config, in the section "[ba]"
  - Public keys for ssh access to the created servers can be placed in ~/.TerraformTemplate/test-env/assets/ba_public_keys.pub and/or in ~/terraform/**[compartment]**/assets/ba_public_keys.pub.
- **Operating system details** (adaptation to own needs possible)
  - see ~/terraform/**[compartment]**/variables.tf # Default Image for compute
  - It came from ~/.TerraformTemplate/test-env/variables.tf (copied by provision_new_compartments.bash)
- **Sizing details** (adaptation to own needs possible)
  - see ~/terraform/**[compartment]**/terraform.tfvars # The Shapes and # ASM Disks
  - It came from ~/.TerraformTemplate/test-env/terraform.tfvars (copied by provision_new_compartments.bash)
- **Script details**
  - all scripts (provision_new_compartments.bash, savepoint.bash, restore.bash) only have standard output stream (e.g. to get a logfile just use "command > file")
  - to enhance output details set DEBUG_PRINT:=true (DEBUG_PRINT="${DEBUG_PRINT:=true}")

### Cloud Resilience Example - Initial run

ssh operation host [operation]

#### connect working compartment to operation host
~~~
#
# initial run (~/terraform is empty)
# --------------------------------------------------------------
cd ~/terraform
ls -lah ~/terraform
provision_new_compartments.bash
~~~

#### Create the new environment from scratch
~~~
# after initial run (~/terraform is not empty)
# --------------------------------------------------------------
ls -lah ~/terraform
ls -lah ~/terraform/Test1
cat  ~/terraform/Test1/terraform.tfvars
cd ~/terraform/Test1
terraform init
# terraform init -upgrade
terraform apply
#
# this "terraform apply" create the following environment
# --------------------------------------------------------------
#     VCN: private 10.0.10.0/24 and public 192.168.4.0/24
# Compute: Bastion, LoadTest, DBServ0, DBServ1
# Storage: Boot Volumes: Bastion (Boot Volume), LoadTest (Boot Volume), DBServ0 (Boot Volume), DBServ1 (Boot Volume)
# Storage: Block Volume attached to DBServ0: asmdisk0.0, asmdisk0.1, asmdisk0.2
# Storage: Block Volume attached to DBServ0: asmdisk0.0, asmdisk0.1, asmdisk0.2
# Storage: Block Volume attached to DBServ1: asmdisk1.0, asmdisk1.1, asmdisk1.2
#
# ONLY if there is already a backup, this will be restored
~~~

#### local and remote backup new environment
~~~
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
# 1. GET information about the environment
# 2. ALL nodes in the compartment are stopped.
#    - So also DBServ0, DBServ1, LoadTest and Bastion
#    - if further nodes have been created in the compartment, they will also be stopped.
# 3. Housekeeping
#    - delete all volume-group backups
#    - delete all Volume Backups in the current compartment
#    - delete all Boot Volume Backups in the current compartment
# 4. DELETE all block volume backups and boot volume backups
#    - NOT JUST boot volume backups of DBServ[01], LoadTest and Bastion
#    - NOT JUST block volume backups of the asmdisk[01].* block volumes attached to DBServ[01].
#    - ALL in the current compartment
# 5. CREATE Boot volume backups of ALL boot volumes in the compartment, including those of DBServ[01], LoadTest, and Bastion
# 6. CREATE Block volume backups of ALL block volumes in the compartment, also from the block volumes asmdisk[01].*
# 7. CREATE Volume Group Backups of ALL Volume Groups (e.g. VGasmdisk0, VGasmdisk1)
# 8. MOVE all backups to DB-Test/Sicherungen/Test1BCKP
# 9. COPY all boot volume backups and Volume Group Backups Cross-Region
#10. STARTUP compute instances in compartment
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
cd ~/terraform/Test1
# use ./savepoint.bash --backupregion to get the usage information of ./savepoint.bash
#
# Usage :
#    savepoint.bash
#    to run backup for the environment in the given region
#
#    savepoint.bash --backupregion <region_name>
#    to run backup for the environment in the local and backup region
#
#    possible region names are:
#    +-----+-------------------+
#    | key | name              |
#    +-----+-------------------+
#    | AMS | eu-amsterdam-1    |
#    | ARN | eu-stockholm-1    |
#    | AUH | me-abudhabi-1     |
#    | BOM | ap-mumbai-1       |
#    | CDG | eu-paris-1        |
#    | CWL | uk-cardiff-1      |
#    | DXB | me-dubai-1        |
#    | FRA | eu-frankfurt-1    |
#    | GRU | sa-saopaulo-1     |
#    | HYD | ap-hyderabad-1    |
#    | IAD | us-ashburn-1      |
#    | ICN | ap-seoul-1        |
#    | JED | me-jeddah-1       |
#    | JNB | af-johannesburg-1 |
#    | KIX | ap-osaka-1        |
#    | LHR | uk-london-1       |
#    | LIN | eu-milan-1        |
#    | MEL | ap-melbourne-1    |
#    | MRS | eu-marseille-1    |
#    | MTZ | il-jerusalem-1    |
#    | NRT | ap-tokyo-1        |
#    | PHX | us-phoenix-1      |
#    | SCL | sa-santiago-1     |
#    | SIN | ap-singapore-1    |
#    | SJC | us-sanjose-1      |
#    | SYD | ap-sydney-1       |
#    | VCP | sa-vinhedo-1      |
#    | YNY | ap-chuncheon-1    |
#    | YUL | ca-montreal-1     |
#    | YYZ | ca-toronto-1      |
#    | ZRH | eu-zurich-1       |
#    +-----+-------------------+
#
./savepoint.bash --backupregion uk-london-1
~~~

#### restore new environment
~~~
# Usage :
#        restore.bash
#        to list the possible restore points (available backups) for the current compartment
#
#        restore.bash --from <compartment_name>
#        to list the possible restore points (available backups) from another compartment
#
#        restore.bash [--from <compartment_name>] --savepoint <savepoint>
#        to restore from a restore point [from another compartment] (restore manual backup for boot and block volumes)
#
#        restore.bash [--from <compartment_name>] --autosavepoint <savepoint>
#        to restore from a restore point [from another compartment] (restore manual backup for boot volumes and make use of crossregion volume group replication for block volumes)
# --------------------------------------------------------------
cd ~/terraform/Test1
./restore.bash
# output example
# --------------------------------------------------------------
# current compartment name : Test1
# MY_BCKP_COMPARTMENT_NAME : Test1BCKP
# found "Test1BCKP" below the "Sicherungen" compartment
# #### Available Savepoints in Compartment Test1BCKP:
# 20220202105456
# 20220202122036
# 20220202143131
./restore.bash --savepoint 20220202143131
# --------------------------------------------------------------
#  ATTENTION: use "-auto-approve" with care
terraform destroy -auto-approve
terraform apply -auto-approve
~~~

#### run your restore in the needed region
  - make sure you have a backup in the needed region (see run your initial backup for details)
  - make sure there is a operation host in needed region
  - recreate the environment
~~~
# eg. [old region]          is eu-frankfurt-1
#     [new region]          is uk-london-1
#     [working compartment] is Test1
# 1) update ~/.oci/config (change only region=[new region] in section [ba])
# 2) update ~/terraform/[working compartment]/terraform.tfvars: region=[new region]
# 3) update ~/terraform/Test1/variables.tf                    : Default Image for compute to [new region]
terraform apply
~~~

#### OCI Block Storage crossregion volume group replication

If you like to see an implementation of Max' [Announcing OCI Block Storage crossregion volume group replication](https://blogs.oracle.com/cloud-infrastructure/post/announcing-oci-block-storage-cross-region-volume-group-replication), please take a look at crossregionVGR.bash at ~/.TerraformTemplate/test-env

Find some screenshots [here][CR1_VGCRR]

~~~
# please choose your compartment (e.g. Test1 in my example) and your compartment OCID.
cd ~/terraform/Test1
# to ACTIVATE OCI Block Storage crossregion volume group replication
./crossregionVGR.bash --compartmentOCID ocid1.compartment.oc1..aaaaaaaaribde6vu4uflb6byvwfc56hgdmfmkr4ztgkjuk237roq64qdlcla --crossregionACTION YES
#
# to DE-ACTIVATE OCI Block Storage crossregion volume group replication
./crossregionVGR.bash --compartmentOCID ocid1.compartment.oc1..aaaaaaaaribde6vu4uflb6byvwfc56hgdmfmkr4ztgkjuk237roq64qdlcla --crossregionACTION NO
~~~

#### clean up

If you like to clean up (delete all backups in a given set of regions), feel free to use the commands (just copy & paste in your bash terminal) below.

~~~
# COMPARTMENT_ID="please_update"
# for PROFILE in [region 1] ... [region n]
# e.g.
# --------------------------------------------------------------------------------
Color_Off='\e[0m'       # Text Reset
Black='\e[0;30m'        # Black
Red='\e[0;31m'          # Red
Green='\e[0;32m'        # Green
Yellow='\e[0;33m'       # Yellow
Blue='\e[0;34m'         # Blue
Purple='\e[0;35m'       # Purple
Cyan='\e[0;36m'         # Cyan
White='\e[0;37m'        # White

# compartment Test1
COMPARTMENTTEST1_ID="ocid1.compartment.oc1..aaaaaaaaribde6vu4uflb6byvwfc56hgdmfmkr4ztgkjuk237roq64qdlcla"
# compartment Test1BCKP
COMPARTMENT_ID="ocid1.compartment.oc1..aaaaaaaagxbcftwhqjvnytmhbmjpfnpq3cquzbbjftqg5lpdujqeppixbowa"

for PROFILE in london germany
do
  echo -e  "$Color_Off"
  echo -e  "$Green delete all backups in profile $PROFILE"
  echo -e  "$Green ================================================================================================================================"

  echo -e  "$Cyan ### delete all BOOT volume backups"
  BACKUP_LIST=/tmp/backup.list
  oci --profile "${PROFILE}" bv boot-volume-backup list --all --compartment-id "${COMPARTMENT_ID}" | jq --raw-output '[.data[] | select(."lifecycle-state"=="AVAILABLE")]' > "${BACKUP_LIST}"
  num_vols=$(( $(jq --raw-output 'length' "${BACKUP_LIST}") + 0))
  echo -e "$Cyan - will delete $num_vols boot volume backups"
  for ((i=0; i<$num_vols; i++)); do
    BV_ID=$(jq --raw-output '.['$i'] | ."id"' "${BACKUP_LIST}")
    echo -e  "$Cyan - oci --profile \"${PROFILE}\" bv boot-volume-backup delete --force --boot-volume-backup-id \"${BV_ID}\" --wait-for-state TERMINATED"
    oci --profile "${PROFILE}" bv boot-volume-backup delete --force --boot-volume-backup-id "${BV_ID}" --wait-for-state TERMINATED
  done

  echo -e  "$Cyan ### delete all block volumes in $PROFILE"
  oci --profile "${PROFILE}" bv volume list --all --compartment-id "${COMPARTMENTTEST1_ID}" | jq --raw-output '[.data[] | select(."lifecycle-state"=="AVAILABLE")]' > "${BACKUP_LIST}"
  num_vols=$(( $(jq --raw-output 'length' "${BACKUP_LIST}") + 0))
  echo -e "$Cyan - will delete $num_vols block volumes"
  for ((i=0; i<$num_vols; i++)); do
    BV_ID=$(jq --raw-output '.['$i'] | ."id"' "${BACKUP_LIST}")
    echo -e  "$Cyan - oci --profile \"${PROFILE}\" bv volume delete --force --volume-id \"${BV_ID}\" --wait-for-state TERMINATED"
    oci --profile "${PROFILE}" bv volume delete --force --volume-id "${BV_ID}" --wait-for-state TERMINATED
  done

  echo -e  "$Cyan ### delete all VOLUME-GROUP backups"
  oci --profile "${PROFILE}" bv volume-group-backup list --all --compartment-id "${COMPARTMENT_ID}" | jq --raw-output '[.data[] | select(."lifecycle-state"=="AVAILABLE")]' > "${BACKUP_LIST}"
  num_vols=$(( $(jq --raw-output 'length' "${BACKUP_LIST}") + 0))
  echo -e "$Cyan - will delete $num_vols volume group backups"
  for ((i=0; i<$num_vols; i++)); do
    BV_ID=$(jq --raw-output '.['$i'] | ."id"' "${BACKUP_LIST}")
    echo -e "$Cyan - oci --profile \"${PROFILE}\" bv volume-group-backup delete --force --volume-group-backup-id \"${BV_ID}\" --wait-for-state TERMINATED"
    oci --profile "${PROFILE}" bv volume-group-backup delete --force --volume-group-backup-id "${BV_ID}" --wait-for-state TERMINATED
  done
  oci --profile "${REPLICA_PROFILE}" bv volume-group-replica list --compartment-id "${COMPARTMENT_ID}" --availability-domain "${REPLICA_AD_NAME}" --lifecycle-state "AVAILABLE" > "${BACKUP_LIST}"
  num_vols=$(( $(jq --raw-output 'length' "${BACKUP_LIST}") + 0))
  echo -e "$Cyan - found $num_vols volume group replica"

  echo -e  "$Cyan done in profile $PROFILE"
  echo -e  "$Color_Off "
done
~~~


<!--- Links -->

[home]:                        https://gitlab.com/hmielimo/next-generation-cloud
[CR1_MK]:                      https://www.performing-databases.com/en/unternehmen/martin-klier/
[CR1_MKK]:                     https://www.doag.org/de/home/news/doag-datenbank-kolumne-cloud-multis-und-multi-cloud/detail/
[CR1_homeregion]:              https://docs.oracle.com/en-us/iaas/Content/Identity/Tasks/managingregions.htm
[CR1_costanalyse]:             https://docs.oracle.com/en-us/iaas/Content/Billing/Concepts/costanalysisoverview.htm
[CR1_costexample]:             ../images/cost.perspective.overview.jpg
[CR1_example]:                 ../images/cloud.resilience.example.jpg
[CR1_exampleoverview]:         ../images/cloud.resilience.example.overview.jpg
[CR1_exampleserver]:           ../images/cloud.resilience.example.server.detail.jpg
[CR1_exampleprocess]:          ../images/cloud.resilience.example.process.workflow.jpg
[CR1_VGCRR]:                   ../images/OCI_Block_Volume_Group_Cross-Region_Replication_public.pdf
[CR1_wp1]:                     ../images/deploying.custom.os.images.pdf
[CR1_wp2]:                     https://docs.cloud.oracle.com/en-us/iaas/Content/Block/Tasks/cloningabootvolume.htm
[CR1_wp3]:                     https://docs.cloud.oracle.com/en-us/iaas/Content/Compute/Tasks/recoveringlinuxbootvolume.htm
[CR1_wp4]:                     https://docs.cloud.oracle.com/en-us/iaas/Content/Block/Tasks/backingupavolume.htm
[CR1_wp5]:                     https://docs.cloud.oracle.com/en-us/iaas/Content/Block/Tasks/cloningavolume.htm
[CR1_wp6]:                     https://blogs.oracle.com/cloud-infrastructure/introducing-volume-groups:-enabling-coordinated-backups-and-clones-for-application-protection-and-lifecycle-management
[CR1_wp7]:                     https://docs.oracle.com/en/solutions/oci-best-practices-resilience/reliable-and-resilient-cloud-topology-practices1.html#GUID-886CD0D4-B258-4105-A9AD-C84AE9B3E2A9
[CR1-keygen]:                  https://standby.cloud/ssh-keygen/
[CR1_wp8]:                     https://docs.oracle.com/en-us/iaas/Content/Block/Concepts/volumegroupreplication.htm
[CR1_wp9]:                     https://docs.oracle.com/en/solutions/bare-metal-db-autofailover
[CR1_wp10]:                    https://docs.oracle.com/en/solutions/deploy-tomcat-mysql
[CR1_wp11]:                    https://docs.oracle.com/en/solutions/deploy-microsoft-sql-on-oci
[CR1_wp12]:                    https://apexapps.oracle.com/pls/apex/f?p=133:180:6181540747513::::wid:724


<!-- /Links -->
