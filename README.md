# Cloud Resilience

## Getting Started with Oracle Cloud Infrastructure (OCI)
Over the last two decades Cloud Computing has revolutionized the technological landscape, allowing companies to rent infrastructure instead of building rapidly-obsolescing hardware on-site. During this time, the architectural approach that combines infrastructure orchestration and application code into cloud services has evolved. Today, cloud user have a choice between technology stacks for virtual hosts, master-slave architectures and container cluster. Each stack wraps application code differently, relies on different methods to launch server and provides different mechanism to automate fail-over and scaling processes. For Enterprise IT organizations, managing a large variety of applications, choosing one particular stack is usually not sufficient, because a single stack can not address the broad variety of functional and non-functional requirements. Multi-cloud, hence spreading workloads across multiple cloud provider is a common strategy to address this constrain. However, deploying private workloads across multiple public infrastructure stacks increases operational complexity significantly and comes with certain vulnerabilities. Adopting a second generation infrastructure service (IaaS) like [Oracle Cloud Infrastructure (OCI)][CR_oci]
 is an attractive alternative. Purpose build hardware enables cloud providers to separate the orchestration layer from the hosts and allows cloud customers to build private data centers on prebuild infrastructure pools. Programming interfaces allow operators to build extensible service delivery platforms and service owners to modernize applications in incremental steps. The bare metal approach allows to run enterprise applications in a traditional way. Cloud orchestrator's remain optional and can be added as managed service, but even than the user remain in control over the infrastructure resources.

[<img alt="Global Presence" src="https://www.oracle.com/a/ocom/img/rc24-oci-region-map.png" title="Data Center Regions" width="80%">][CR_ocilandscape]

Oracle operates a fast growing network of data center to provide access to prebuild cloud infrastructure in more than 30 regions. In addition, Oracle builds private infrastructure pools on-demand and offers to extend these pools with edge compute or database nodes. In every data center, dedicated compute- and storage resources are isolated on a native layer three network. Orchstrators incl. hypervisor, container and network functions remain private by default - also in shared pools. Combining on open-source orchestration technologies with cloud agnostic monitoring and management services allows operators to build a control center for hybrid cloud services. End to end programmability ensures fast and flexible resource deployments, with platform components like middleware, integration and database infrastructure, provided either as managed or as unmanaged service to offer a choice between convenience and control. In any case, standard hardware controls like the [integrated lights out manager (ILOM)][CR_ilom] and [off-box vitrualization][CR_offboxvirt] allow to address regional privacy regulations and compliance requirements.

# Improve your resilience in Oracle Cloud Infrastructure

## [What is Cloud Resilience][CR_cloudresilience]

The architecture of a reliable application in the cloud is typically different from a traditional application architecture. While historically you may have purchased redundant higher-end hardware to minimize the chance of an entire application platform failing, in the cloud, it's important to acknowledge up front, that failures will happen. Instead of trying to prevent failures altogether, the goal is to minimize the effects of a single failing component (SPOF). Follow these best practices to build reliability into each step of your design process.

Reliable applications are:

- Resilient and recover gracefully from failures, and they continue to function with minimal downtime and data loss before full recovery.
- Highly available (HA) and run as designed in a healthy state with no significant downtime.
- Protected from Region failure through good disaster recovery (DR) design.

Understanding how these elements work together, and how they affect cost, is essential to building a reliable application. It can help you determine how much downtime is acceptable, the potential cost to your business, and which functions are necessary during a recovery.

Beside all Oracle Cloud Infrastructure benefits (find them below) we focus here on **[Cloud Resilience][CR_CR]** to "Keep your systems running".

Finally, if you need additional support - we help!
- Contact [Oracle Consulting][CR_consulting] e.g. regarding individual implementation requirements
- Contact [Oracle Software Investment Advisory][CR_sia] e.g. regarding your Financial Management, Reporting and Tooling requirements
- Contact [Oracle Sales][CR_sales] regarding all other topics and e.g. your bonus: [Oracle Cloud Optimize Series Workshops][CR_workshop] Four 2h workshops based on [Best practices framework for Oracle Cloud Infrastructure][CR_bestpractice] to address your business goals in
  - Security and compliance
  - Reliability and resilience
  - Performance and cost optimization
  - Operational efficiency

# Oracle Cloud Infrastructure Benefits

## Autonomous services
OCI is the exclusive home of Oracle Autonomous Database and its self-repairing, self-optimizing autonomous features. Leveraging machine learning to automate routine tasks, Autonomous Database delivers higher performance, better security, and improved operational efficiency, and frees up more time to focus on building enterprise applications.

- [Gartner: Critical Capabilities for Operational Database Management Systems][CR_gartner1]

## Reduce Costs and enhance performance
Oracle Cloud Infrastructure is built for enterprises seeking higher performance, lower costs, and easier cloud migration for their existing on-premises applications, and better price-performance for cloud native workloads. Read how customers have moved from AWS to Oracle Cloud Infrastructure, substantially reducing their costs and enhancing their compute platform performance.

- [Compare against AWS][CR_compareaws]
- [Read Gartners perspective on Oracles public cloud][CR_gartner2]

## Easily migrate enterprise apps
Traditional, on-premises workloads that enterprises rely on to run their business are easier to migrate to Oracle Cloud. Designed to deliver bare-metal compute services, network traffic isolation, and the only service-level guarantees for performance, Oracle Cloud enables rapid migration and faster time to innovation. Build new value around migrated applications faster with Autonomous Database, data science tools, and our cloud native development tools.

- [Learn why Oracle apps run best on OCI][CR_bestonoci]
- [Start migrating your custom apps to OCI][CR_migrate]

## Best support for hybrid architecture
Deploy your cloud applications and databases anywhere with a wide choice of options, ranging from our public regions to edge devices. In addition to our public cloud region, we offer full private Dedicated Regions in customers data centers, edge-computing Oracle Roving Edge devices, and our blazingly fast Oracle Exadata Cloud@Customer, with Autonomous Database service delivered behind your firewall. With full support for VMware environments in the customer tenancy as well, Oracle offers cloud computing that works the way you expect.

- [Learn about hybrid, multicloud, and intercloud deployment options][CR_multicloud]
- [Oracle Brings the Cloud to You (PDF)][CR_tocloud]

Oracle Cloud Infrastructure (OCI) is a deep and broad platform of public cloud services that enables customers to build and run a wide range of applications in a scalable, secure, highly available, and high-performance environment.
For on-premises requirements, OCI is available with the new Dedicated Region Cloud@Customer—behind a company’s private firewall and in their data center.
A detailed "Getting Started Guide" is part of our documentation and available here: [Getting Started with OCI][CR_gettingstarted]

Cloud operations engineering is a relatively new field extending the scope of [IT service management (ITSM)][CR_itsm]. It represents an important step towards more agility and flexibility in service operation. The concept of "Infrastructure as Code" replaces runbook tools and has become an enabler for self-service delivery - even for complex solution architectures. Operators empower service owners and developers to add, change or delete infrastructure on demand with deployment templates for logical resources. Service consumers gain flexibility to provision virtual infrastructure while resource operators remain in control of the physical infrastructure. This repositories ([The OCloud Framework: Landing Zone][CR_ocloud], [OCloud Tutorial][CR_ocloudt] and [CIS OCI Landing Zone Quick Start Template][CR_csilandingzone]) aims to provide a path for IT Organizations introducing cloud engineering. We starts with a short introduction about Infrastructure as Code, show how to define logical resources for application and database services and end with as an example how to consolidate infrastructure and application services in a self-service catalog. We build on the official [Oracle Cloud Architect training][CR_training], that prepares administrators for the [Oracle Architect Associate exam][CR_exam], but aim to extend the [Learning Path][CR_laerningpath], with tool recommendations and code examples for cloud engineers.

---

<!--- Links -->

[home]:                        /README.md
[CR_oci]:                     https://www.oracle.com/cloud/
[CR_ocilandscape]:            https://www.oracle.com/cloud/architecture-and-regions.html
[CR_cloudresilience]:         https://docs.oracle.com/en/solutions/oci-best-practices-resilience/reliable-and-resilient-cloud-topology-practices1.html#GUID-14AE6070-E47B-412D-B316-8CCDF36268E9
[CR_CR]:                      doc/cloud.resilience/README.md
[CR_ilom]:                    https://www.oracle.com/servers/technologies/integrated-lights-out-manager.html
[CR_offboxvirt]:              https://blogs.oracle.com/cloud-infrastructure/first-principles-l2-network-virtualization-for-lift-and-shift
[CR_gartner1]:                https://www.oracle.com/database/gartner-dbms.html
[CR_compareaws]:              https://www.oracle.com/cloud/economics/
[CR_gartner2]:                https://www.oracle.com/cloud/gartner-oci.html
[CR_bestonoci]:               https://www.oracle.com/cloud/migrate-applications-to-oracle-cloud/
[CR_migrate]:                 https://www.oracle.com/cloud/migrate-custom-applications-to-cloud/
[CR_multicloud]:              https://www.oracle.com/cloud/cloud-deployment-models/
[CR_tocloud]:                 https://www.oracle.com/a/ocom/docs/engineered-systems/exadata/idc-adb-on-exac-at-cloud.pdf
[CR_gettingstarted]:          https://docs.oracle.com/en-us/iaas/Content/GSG/Concepts/baremetalintro.htm
[CR_itsm]:                    https://en.wikipedia.org/wiki/IT_service_management
[CR_ocloud]:                  https://github.com/oracle-devrel/terraform-oci-ocloud-landing-zone
[CR_ocloudt]:                 https://cool.devo.build/tutorials/7-steps-to-oci/
[CR_csilandingzone]:          https://github.com/oracle-quickstart/oci-cis-landingzone-quickstart
[CR_training]:                https://www.oracle.com/cloud/iaas/training/
[CR_exam]:                    https://www.oracle.com/cloud/iaas/training/architect-associate.html
[CR_laerningpath]:            https://learn.oracle.com/ols/learning-path/become-oci-architect-associate/35644/75658
[CR_7steps]:                  https://go.oracle.com/LP=115319
[CR_consulting]:              https://www.oracle.com/consulting/
[CR_sia]:                     https://www.oracle.com/corporate/software-investment-advisory/
[CR_sales]:                   https://www.oracle.com/corporate/contact/
[CR_workshop]:                doc/images/Cloud_Optimize_Series_Workshop.pdf
[CR_bestpractice]:            https://docs.oracle.com/en/solutions/oci-best-practices/


<!-- /Links -->
